#!/bin/bash

# this script execute the deployment of the Provisioner
#
# author: yari.latini@synchronoss.com

# REINSTALL PROCEDURE
#
# in order to reset the changes from this playbook, execute the command below (stop primary as last if you're reinstalling everything):
#
# ansible -i <inventory-file> -b -f1 -m shell -a 'systemctl stop provisioner' <target_host>
#
# remove package
# ansible -i <inventory-file> -b -f1 -m shell -a 'rm -rf /opt/imail/provREST' <target-host>

# different playbook could have different tags from Merlin you may want to skip
# set below the comma separated list of tags you want to ignore
TAGBL="--skip-tags=hostname,timezone,userpermissions,users,ntp,dns,limits,hosts"
# this is the playbook you will execute
PLAYBOOK="morgana-provisioner.yml"

# parse options and execute
source $(dirname $0)/lib.sh

# Useful commands
#
# START / STOP SERVICES ================================================
#
# Restart the instances
# ansible -i <inventory-file> -b -f1 -m shell -a 'systemctl stop provisioner' <target_host>
#
# MONITORING COMMANDS ==================================================
#
# ansible -i <inventory-file> -b -f1 -m shell -a 'runuser -l imail -c "/opt/imail/provREST/provREST/provREST.sh status"' <target-host>
#
# API tests IN PROGRESS
#Authenticate (from provisioner host);
# curl -c /tmp/`echo .$$`  -b /tmp/`echo .$$` -v -X POST http://localhost:18680/prov/authenticate -d "user=superadmin@mx.internal&password=password"
#
#Domain cmd:
# curl -c /tmp/`echo .$$`  -b /tmp/`echo .$$` -H 'User: superadmin@mx.internal' -v -X GET http://localhost:18680/prov/domain/mx.internal
#
#Admin cmd:
# curl -c /tmp/`echo .$$`  -b /tmp/`echo .$$` -H 'User: superadmin@mx.internal' -v -X POST http://localhost:18680/prov/domain/mx.internal/user/admin -d "password=password&cosid=default&adminrole=ProfileDomainAdmin&adminObject=mx.internal"
#
#User cmd:
#
#curl -c /tmp/`echo .$$`  -b /tmp/`echo .$$` curl -v -X GET http://172.26.202.12:18680/prov/domain/mx.internal/user/testX
#curl -c /tmp/`echo .$$`  -b /tmp/`echo .$$` curl -v -X POST http://172.26.202.12:18680/prov/domain/mx.internal/user/testX -d "password=password&cosid=default"
#
