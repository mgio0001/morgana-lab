#!/bin/bash

# this script sets up the directory servers for automatic failover
#
# author: gary.palmer@synchronoss.com

# Before this script, please execute all scripts up to and including morgana-10-dirserv.sh
# It is also required to have Configuration Service already installed (prod01-confserv.sh)
#
# Applies or updates basic configuration on the managed hosts including:
# 1) modifying configdb and starting services

# different playbook could have different tags from Merlin you may want to skip
# set below the comma separated list of tags you want to ignore
TAGBL="--skip-tags=hostname,timezone,userpermissions,users,ntp,dns,limits,hosts"
# this is the playbook you will execute
PLAYBOOK="morgana-dirserv-ha.yml"

# parse options and execute
source $(dirname $0)/lib.sh

# Useful commands
#
# START / STOP SERVICES ================================================
#
# Restart the instances (important -f1, stop primary as last and start primary as first)
# ansible -i <inventory-file> -b -f1 -m shell -a 'systemctl restart mx' <target_host>
# ansible -i <inventory-file> -b -f1 -m shell -a 'runuser -l imail -c "/opt/imail/lib/imservctrl restart imdirserv"' directory
# ansible -i <inventory-file> -b -f1 -m shell -a 'runuser -l imail -c "/opt/imail/lib/imservctrl stop imdirserv"' directory
# ansible -i <inventory-file> -b -f1 -m shell -a 'runuser -l imail -c "/opt/imail/lib/imservctrl start imdirserv"' directory
#
# MONITORING COMMANDS ==================================================
#
# Check config.db alignment
# ansible -i <inventory-file> -b -m shell -a 'runuser -l imail -c "md5sum config/config.db"' directory,secondarydirectory -o
#
# Check processes (imconfserv and immgrsev MUST be up and running)
# ansible -i <inventory-file> -b -m shell -a 'runuser -l imail -c imservping' directory
#
# Check replicas (all the installed directory must be present and aligned
# ansible -i <inventory-file> -b -m shell -a 'runuser -l imail -c imdirrcstatusrpt' directory
#
# Check diskspace
# ansible -i <inventory-file> -b -m shell -a 'df -h /opt/imail/scdb' directory,secondarydirectory
#
# Check current log files for Errors/Warning/Critical messages
# ansible -i <inventory-file> -b -m shell -a 'runuser -l imail -c "find log/ -name \*.log -type l | xargs grep -v Note | imlogprint -m"' <target-host>
#
# CONFIGURATION CHECKS =================================================
#
# Check ports
# ansible -i <inventory-file> -b -m shell -a 'runuser -l imail -c "imconfcontrol -ports"' directory
# output:
# port range     host                 module               config parm
# ============   ==================== ==================== ====================
#   6000         *                    immgrserv            adminPort
#   6001         *                    immgrserv            mgrServPort
#   6002         *                    imconfserv           adminPort
#   6003         *                    imconfserv           confServPort
#   6004         *                    imdirserv            adminPort           
#   6005         *                    imdirserv            dirRMEPort          
#   6006         *                    imdirserv            ldapPort
#

