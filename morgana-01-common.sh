#!/bin/bash

# this script executes the configuration and 
# pre-installation tasks on the managed nodes. 
# it has to be executed on all the managed nodes before the others scripts.
#
# author: yari.latini@synchronoss.com
# doc: https://bitbucket.org/openwave/morgana/wiki/morgana-01-common.sh

# Applies or updates basic configuration on the managed hosts including:
# 1) /etc/hosts entries accordingly with the inventory file and the host_vars/*
# 2) ntp configuration 
# 3) dns configuration
# 4) services users and groups
# 5) homes / mounts permissions
# 6) timezones configuration
# 

# different playbook could have different tags from Merlin you may want to skip
# set below the comma separated list of tags you want to ignore
TAGBL=""
# this is the playbook you will execute
PLAYBOOK="morgana-common.yml"

# parse options and execute
source $(dirname $0)/lib.sh
