#!/bin/bash

# this is a generic script helping to execute
# ansible-playbook and playbook.
#
# The other deployment script will source this script providing only
# variable in order to point to the right playbook (usually morgana-*.yml)
# and a list of tags to skip during the execution.
#
# author: yari.latini@synchronoss.com

PROJECT_ROOT=$(dirname $0)
if [ "." == "$PROJECT_ROOT" ]; then
	PROJECT_ROOT=$(pwd)
fi
source $(dirname $0)/cfg/morgana.cfg

BIN=$(which ansible-playbook)
# because of the confserv it is better to avoid parallel tasks
#OPTS="-b -f1"
OPTS="-b"
TARGET=""
TAGS=""
SSHKEYGEN=$(which ssh-keygen)

if [ -n "$EXTRA_OPTS" ]; then
	OPTS="${OPTS} ${EXTRA_OPTS}"
fi

if [ -z "$BIN" ] || [ ! -f "$BIN" ]; then
  echo "ansible-playbook not found, please verify your setup"
  exit 1
fi

if [ ! -f "$PRIVATE_KEY_FILE" ] && [ -f "$SSHKEYGEN" ]; then
	echo "Missing private key ${PRIVATE_KEY_FILE}"
	echo "Do you want to create it? (yes/no)"
	read res
	if [ "yes" == "$res" ]; then
		$SSHKEYGEN -t rsa -b 4096 -f ${PRIVATE_KEY_FILE}
		if [ $? != 0 ]; then
			echo "Error while creating private key: $?"
			exit 1
		fi
	else
		exit 1
	fi
fi

if [ ! -d "$(dirname $ANSIBLE_LOG_FILE)" ]; then
	mkdir -p $(dirname $ANSIBLE_LOG_FILE)
	if [ $? != 0 ]; then
		echo "Can't create log path"
		exit 1
	fi
fi

if [ ! -f "$PRIVATE_KEY_FILE" ]; then
	echo "Can't access the private key $PRIVATE_KEY_FILE"
	exit 1
fi

PRIVATE_KEY_FILE_PERMS=$(stat -c "%a" ${PRIVATE_KEY_FILE})
if [ "x${PRIVATE_KEY_FILE_PERMS}" != "x600" ]; then
    chmod 600 ${PRIVATE_KEY_FILE}
fi

while [[ $# -gt 0 ]]
do
key="$1"

  case $key in
    -c|--check)
    CHECK=1
    OPTS=$OPTS" --check --force-handlers -e merlin_testing=true"
    shift
    ;;
    -t|--target)
    shift
    TARGET=" --limit=$1"
    shift
    ;;
    --list-hosts)
    OPTS=$OPTS" --list-hosts"
    shift
    ;;
    --list-tasks)
    OPTS=$OPTS" --list-tasks"
    shift
    ;;
    --list-tags)
    OPTS=$OPTS" --list-tags"
    shift
    ;;
    --tags)
    shift
    TAGS=" --tags=$1"
    shift
    ;;
    --skip-tags)
    shift
    if [ -z "$TAGBL" ]; then
       TAGBL="--skip-tags=$1"
    else
       TAGBL="${TAGBL},$1"
    fi
    shift
    ;;
    --inventory)
    shift
    if [ -f "$1" ]; then
        INVENTORY=$1
    else
        INVENTORY=${PROJECT_ROOT}/$1
    fi
    shift
    ;;
    --setenv)
    shift
    SETENV=1
    ;;
    --ssh)
    shift
    SSHHOSTID=$1
    shift
    ;;
    --verbose)
    OPTS=$OPTS" -vvv"
    shift
    ;;
    *)
    HELP=1
    shift
    ;;
  esac

done;

if [ "1" == "$HELP" ]; then
  echo "Usage: "
  echo "$0 [options]"
  echo "options:"
  echo " --check - execute tasks by not performing any change"
  echo " --inventory - the inventory to use"
  echo " --target <hostid|groupid> - execute tasks on the specified hosts / groups (support wildcards)"
  echo " --list-hosts - prints the list of the hosts affected by the tasks and exit (no change performed)"
  echo " --list-tasks - prints the list of the tasks executed on the hosts and exit (no change performed)"
  echo " --list-tags - prints the list of the tags executed on the and exit (no change performed)"
  echo " --tags - execute only the specified tags (useful for update, verify which tags you need with --list-tags / --list-tasks, before proceeding)"
  echo " --skip-tags - skip the specified tags for the execution, useful to refine deployment, update configurations..."
  echo " --setenv - export environment variables for ansible tool"
  echo " --verbose - Increase verbosity on the output of the command"
  echo " --ssh <hostid> - Open an SSH session like ansible does"
 exit 1;
fi

if [ -z "$SETENV" ] && [ -z "$SSHHOSTID" ] ; then

	if [ ! -f "$INVENTORY" ] && [ ! -d "$INVENTORY" ]; then
	  echo "Missing inventory: $INVENTORY"
	  exit 1
	else
	  INVENTORY="-i $INVENTORY"
	fi

	PLAYBOOK="${PLAYBOOKS_ROOT}/${PLAYBOOK}"

	if [ ! -f "$PLAYBOOK" ]; then
	  echo "Missing playbook: $PLAYBOOK"
	  exit 1
	fi

        if [ -n "$COMMON_SKIP_LIST" ]; then
		if [ -z "$TAGBL" ]; then
			TAGBL="--skip-tags=${COMMON_SKIP_LIST}"
		else
			TAGBL="${TAGBL},${COMMON_SKIP_LIST}"
		fi
	fi

	echo "ENV: ANSIBLE_CONFIG=$ANSIBLE_CONFIG PROJECT_ROOT=$PROJECT_ROOT ANSIBLE_LOG_FILE=$ANSIBLE_LOG_FILE PRIVATE_KEY_FILE=$PRIVATE_KEY_FILE"
	echo "Execute Ansible Command:"
	echo $BIN $INVENTORY $PLAYBOOK $OPTS $TAGBL $TARGET $TAGS
	ANSIBLE_CONFIG=$ANSIBLE_CONFIG PROJECT_ROOT=$PROJECT_ROOT ANSIBLE_LOG_FILE=$ANSIBLE_LOG_FILE PRIVATE_KEY_FILE=$PRIVATE_KEY_FILE ANSIBLE_CACHE_PATH=$ANSIBLE_CACHE_PATH $BIN $INVENTORY $PLAYBOOK $OPTS $TAGBL $TARGET $TAGS

elif [ -n "$SSHHOSTID" ]; then

	SSHUSER=$(grep remote_user $ANSIBLE_CONFIG | awk -F "=" '{ print $2 }')
	OPTS="-o StrictHostKeyChecking=no -o ServerAliveInterval=60"
	CMD="ssh -i ${PRIVATE_KEY_FILE} ${OPTS} -l ${SSHUSER} ${SSHHOSTID}"
	$CMD

else

	echo "ANSIBLE_CONFIG=$ANSIBLE_CONFIG PROJECT_ROOT=$PROJECT_ROOT ANSIBLE_LOG_FILE=$ANSIBLE_LOG_FILE PRIVATE_KEY_FILE=$PRIVATE_KEY_FILE ANSIBLE_CACHE_PATH=$ANSIBLE_CACHE_PATH"

fi
