#!/bin/bash

ansible-playbook -i /opt/merlin/staging --limit=auhstg -b /opt/merlin/cassblob.yml --skip-tags=epel,owm-repo,remi-repo,iptables_disable,selinux_disable,owm-logstash-repo
