#!/bin/bash

# this script execute the deployment of the pab and cal meta rings
#
# author: yari.latini@synchronoss.com

# Before this script, please execute prod00-common.sh
# e.g.
# ./prod00-common.sh --target pabcalcassmeta

# Applies or updates basic configuration on the managed hosts including:
# 1) execute common tasks (same as prod00-common)
# 2) install and configure cassandra rings for pab and cal

# REINSTALL PROCEDURE
#
# in order to reset the changes from this playbook, execute the command below:
# kill the cassandra processes
#
# ansible -i production -b -m shell -a 'systemctl stop cassmeta' -b <target_host>
#
# remove data and accounts 
# ansible -i production -b -m user -a 'name=cass state=absent remove=yes' -b <target_host>
# for pimgdb-* perform also this 
# ansible -i production -b -m shell -a 'rm -rf /opt/cassmeta/{.cassandra,cassandra-meta,.oracle_jre_usage,scripts}' -b 

# different playbook could have different tags from Merlin you may want to skip
# set below the comma separated list of tags you want to ignore
TAGBL="--skip-tags=owm-queue-service,owm-common,epel,nrpe,selinux_disable,iptables_disable,owm-repo,hostname,timezone,userpermissions,users,ntp,dns,limits,hosts"
# this is the playbook you will execute
PLAYBOOK="/opt/merlin/morgana-pabcalcassmeta.yml"

# parse options and execute
source /opt/merlin/lib.sh

# Useful pab/cal meta commands
#
# TEST MAINTENANCE =====================================================
#
# Execute the maintenance scripts node by node (recommended after the deployment in order to test the nodes and their maintenance)
# ansible -i production -b -m shell -a 'bash {{cass_user[0].home}}/scripts/cassmeta_maintenance.sh' pabcalcassmeta -f1
#
# Check cronjobs have been installed and scheduled properly (very weekday are well distributed among nodes and servers)
# ansible -i production -b -m shell -a 'cat /var/spool/cron/{{cass_user[0].username}}' pabcalcassmeta -f1
#
# Update only the maintenance scripts
# ./prod01-mssmetacass.sh --tags maintenance,owm-cassmeta-init
#
# Update the configuration files
# ./prod01-mssmetacass.sh --tags owm-cassmeta-init,owm-cassmeta-confupdates
#
# START / STOP SERVICES ================================================
#
# Restart the instances
# ansible -i production -b -m shell -a 'systemctl restart cassmeta' pabcassmetamss
#
# Execute nodetool <command>
# ansible -i production -b -m shell -a 'runuser -l {{cass_user[0].username}} -c "cassandra-meta/cassandra/bin/nodetool <command>"' pabcalcassmeta
#
# MONITORING COMMANDS ==================================================
#
# Check ring status
# ansible -i production -b -m shell -a 'runuser -l {{cass_user[0].username}} -c "cassandra-meta/cassandra/bin/nodetool ring pab"' pimdb-a-01
# ansible -i production -b -m shell -a 'runuser -l {{cass_user[0].username}} -c "cassandra-meta/cassandra/bin/nodetool ring cal"' pimdb-a-01
#
# Check compaction status
# ansible -i production -b -m shell -a 'runuser -l {{cass_user[0].username}} -c "cassandra-meta/cassandra/bin/nodetool compactionstats"' pabcalcassmeta
#
# Check thread pool statistics
# ansible -i production -b -m shell -a 'runuser -l {{cass_user[0].username}} -c "cassandra-meta/cassandra/bin/nodetool tpstats"' pabcalcassmeta
#
# Check disk usage
# ansible -i production -b -m shell -a '{% for m in data_file_directories %} df -h {{ m }} {% endfor %}' pabcalcassmeta
#
# Check log files
# ansible -i production -b -m shell -a 'runuser -l {{cass_user[0].username}} -c "egrep -v \"INFO|^(\s+)|^java\.\" cassandra-meta/log/system.log"' pabcalcassmeta
#
# Check snapshots
# ansible -i production -b -m shell -a 'runuser -l {{cass_user[0].username}} -c "cassandra-meta/cassandra/bin/nodetool listsnapshots"' pabcalcassmeta
# ansible -i production -b -m shell -a '{% for m in data_file_directories %} find  {{ m }} -name snapshots -type d {% endfor %}' pabcalcassmeta
#
# CONFIGURATION CHECKS =================================================
#
# Check for specific configuration parameters (e.g. seeds)
# ansible -i production -b -m shell -a 'runuser -l {{cass_user[0].username}} -c "grep seeds cassandra-meta/cassandra/conf/cassandra.yaml"' pabcalcassmeta
#
# Check cassandra-topology.properties
# ansible -i production -m shell -a 'grep -v '^#' {{cass_user[0].home}}/cassandra-meta/cassandra/conf/cassandra-topology.properties' pabcalcassmeta -b
#
# Check Keyspace (or run any other cqlsh command)
# ansible -i production -b -m shell -a 'runuser -l {{cass_user[0].username}} -c "cassandra-meta/cassandra/bin/cqlsh {{inventory_hostname}} 9042 -k pab -e DESCRIBE\ KEYSPACE"' <node>
# ansible -i production -b -m shell -a 'runuser -l {{cass_user[0].username}} -c "cassandra-meta/cassandra/bin/cqlsh {{inventory_hostname}} 9042 -k cal -e DESCRIBE\ KEYSPACE"' <node>
