#!/bin/bash

KEY="/opt/merlin/roles/owm-ansibleuser/files/ansible_id_rsa"
USER="ansibleuser"
OPTS="-o StrictHostKeyChecking=no -o ServerAliveInterval=60"

HOST=$1

usage() {
  echo "Missing host, usage: $0 <host or ip> <remotepath> <localpath>"
  exit 1
}

if [ -z "$HOST" ]; then
  usage
fi

if [ -z "$2" ]; then
  usage
fi

if [ -z "$3" ]; then
  usage
fi

CMD="scp -r -i $KEY $OPTS $USER@$HOST:$2 $3"
echo $CMD
$CMD
