#!/bin/bash

KEY="/opt/merlin/roles/owm-ansibleuser/files/ansible_id_rsa"
USER="ansibleuser"
OPTS="-o StrictHostKeyChecking=no -o ServerAliveInterval=60"

HOST=$1

if [ "" == "$HOST" ]; then
  echo "Missing host, usage: $0 <host or ip>"
  exit 0
fi

CMD="ssh -i $KEY $OPTS $USER@$HOST"
echo $CMD
$CMD
