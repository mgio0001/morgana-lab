Intro
=======

This is the Merlin based project for Etisalat deployment.
It is currently under development and aligned/merged with merlin/develop branch.

Wrapping tools
===
These are shell script wrapping the ansible-playbook calls. By default merlin 
project offers a wide number of features and configuration, but they are not always
required. They are named based on their target environment and priority by following the format <env><n>-<key>.sh where:
* env -> the environment (e.g. prod)
* n -> the priority (0 -> highest)
* key -> label/id identifying the managed component

* prod00-common.sh - first command applying all the common package / configuration to every managed node
* prod01-cassblob.sh - deployment script for cassandra blobstore (mss)
* prod01-msscassmeta.sh - deployment script for the cassandra metadata store (mss)
* prod01-pabcalmetacass.sh - deployment script for the cassandra metadata store for pab and cal
* prod01-qservicecass.sh - deployment script for the cassandra metadata and blobstore for stateless queue service
* prod01-elasticsearch.sh - deployment script for the elasticsearch cluster (required by queuesearch and monitoringui)
* prod01-confserv.sh - deployment script for the configuration server
* prod02-dirserv.sh - deployment script for master directory
* prod02-sdiscovery.sh - deployment script for service discovery
* prod03-mos.sh - deployment script for mos
* prod03-mss.sh - deployment script for mss
* prod03-qadminsearch.sh - deployment script for queue admin and queue search 
* prod04-qservice.sh - deployment script for stateless queue service
* prod05-affinity.sh - deployment script for the affinity manager nodes (nginx for sMSS, mos and swift*
* prod06-fep.sh - deployment scripts for the frontend nodes

These scripts source lib.sh that provides the execution logic.
They also include some useful ansible commands in order to check the deployed component (see comments within the scripts).

Etisalat environment doesn't provide direct access to the Internet, so it has been required
to create a local yum repository on the management ndoes that is configured and accessed by
all the managed node. See owm-localrepository role for detail.

It has been also required to managed the ansibleuser accounts required in order to
access the managed nodes, see owm-ansibleuser role for details.

Morgana playbooks
===
With Etisalat deployment we want to focus on the deployment / test of every service. This is the reason
we are using some customized playbooks having the purpose of install a single component at once.

* morgana-common.yml - this is a generic playbook covering all the common configuration. It is executed by prod00-common.sh script
* morgana-cassblob.yml - this is the playbook for the blobstore ring for sMSS. It is executed by prod01-cassblob.sh script
* morgana-msscassmeta.yml - this is the playbook for the metadata ring for sMSS. It is executed by the prod01-msscassmeta.sh script
* morgana-pabcalcassmeta.yml - ths is the playbook for the pab and cal rings. It is executed by the prod01-pabcalmetacass.sh script
* morgana-qservicecass.yml - this is the playbook for the blob and data rings for the stateless queue service. It is executed by the prod01-qservicecass.sh script
* morgana-elasticsearch.yml - this is the playbook for the elasticsearch cluster (for monitorui and queuesearch). It is executed by the prod01-elasticsearch.sh script
* morgana-confserv.yml - this is the playook for the configuration service. It is executed by prod01-confserv.sh script
* morgana-dirserv.yml - this is the playbook for the master directory service. It is executed by prod02-dirserv.sh script
* morgana-mos.yml - this is the playbook for the mos. It is executed by prod03-mos.sh script
* morgana-mss.yml - this is the playbook for the sMSS. It is executed by the prod03-mss.sh script
* morgana-qadminsearch.yml - this is the playbook for the queue admin and queue search services. It is executed by the prod03-qadminsearch.sh
* morgana-qservice.yml - This is the playbook for the queue stateless queue service. It is executed by the prod04-qservice.sh script
* morgana-sdiscovery.yml - This is the playbook for the service discovery. It is executed by the prod02-sdiscovery.sh script.
* morgana-affinity.yml - This is the playbook for the affinity manager nodes. It is executed by the prod05-affinity.sh script.
* morgana-fep.yml - This is the playbook for the frontend nodes. It is executed by the prod06-fep.sh script.