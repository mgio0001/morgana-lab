# Based on the docker connection plugin
#
# Connection plugin for configuring kubernetes containers with kubectl
# (c) 2017, XuXinkun <xuxinkun@gmail.com>
#
# This file is part of Ansible
#
# Ansible is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ansible is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ansible.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = """
    author:
        - xuxinkun
    connection: kubectl
    short_description: Run tasks in kubernetes pods
    description:
        - Run commands or put/fetch files to an existing kubernetes pod container.
    version_added: "2.0"
    options:
      kube_namespace:
        description:
            - The namespace of the pod
        default: ''
        vars:
            - name: ansible_kube_namespace
            - name: ansible_kubectl_namespace
      kubectl_extra_args:
        description:
            - Extra arguments to pass to the kubectl command line
        default: ''
        vars:
            - name: ansible_kubectl_extra_args
      kube_config_file:
        description:
            - The path of the config file of kubectl
        default: ''
        vars:
            - name: ansible_kube_config_file
            - name: ansible_kubectl_config_file
      kube_container:
        description:
            - The container to execute. If the pod has more than one container, the container name must be specified
        default: ''
        vars:
            - name: ansible_kube_container
            - name: ansible_kubectl_container
"""

import distutils.spawn
import os
import os.path
import subprocess

import ansible.constants as C
from ansible.errors import AnsibleError, AnsibleFileNotFound
from ansible.module_utils.six.moves import shlex_quote
from ansible.module_utils._text import to_bytes
from ansible.plugins.connection import ConnectionBase, BUFSIZE


try:
    from __main__ import display
except ImportError:
    from ansible.utils.display import Display
    display = Display()


class Connection(ConnectionBase):
    ''' Local kubectl based connections '''

    transport = 'kubectl'
    has_pipelining = True
    become_methods = frozenset(C.BECOME_METHODS)

    def __init__(self, play_context, new_stdin, *args, **kwargs):
        super(Connection, self).__init__(play_context, new_stdin, *args, **kwargs)

        # Note: kubectl supports running as the user container start.
        # So, it is impossible to set the remote user for kubectl connection
        # right now.

        if 'kubectl_command' in kwargs:
            self.kubectl_cmd = kwargs['kubectl_command']
        else:
            self.kubectl_cmd = distutils.spawn.find_executable('kubectl')
            if not self.kubectl_cmd:
                raise AnsibleError("kubectl command not found in PATH")

        self.name = 'kubectl'

    def _build_exec_cmd(self, cmd):
        """ Build the local kubectl exec command to run cmd on remote_host
        """

        local_cmd = [self.kubectl_cmd]

        if self.get_option("kubectl_extra_args"):
            local_cmd += self.get_option("kubectl_extra_args").split(' ')

        # if namespace is not set, it will be `default`
        if self.get_option("kube_namespace"):
            local_cmd += ['-n', self.get_option("kube_namespace")]

        if self.get_option("kube_config_file"):
            local_cmd += ['--kubeconfig', self.get_option("kube_config_file")]

        # -i is needed to keep stdin open which allows pipelining to work
        local_cmd += ['exec', '-i', self._play_context.remote_addr]

        # if the pod has more than one container, the container name must be
        # specified.
        if self.get_option("kube_container"):
            local_cmd += ['-c', self.get_option("kube_container")]

        local_cmd += ['--'] + cmd

        return local_cmd

    def _connect(self, port=None):
        """ Connect to the container. Nothing to do """
        super(Connection, self)._connect()
        if not self._connected:
            display.vvv(u"ESTABLISH kubectl CONNECTION", host=self._play_context.remote_addr)
            self._connected = True

    def exec_command(self, cmd, in_data=None, sudoable=False):
        """ Run a command on the kubectl host """
        super(Connection, self).exec_command(cmd, in_data=in_data, sudoable=sudoable)

        local_cmd = self._build_exec_cmd([self._play_context.executable, '-c', cmd])

        display.vvv("EXEC %s" % (local_cmd,), host=self._play_context.remote_addr)
        local_cmd = [to_bytes(i, errors='surrogate_or_strict') for i in local_cmd]
        p = subprocess.Popen(local_cmd, shell=False, stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        stdout, stderr = p.communicate(in_data)
        return (p.returncode, stdout, stderr)

    def _prefix_login_path(self, remote_path):
        ''' Make sure that we put files into a standard path

            If a path is relative, then we need to choose where to put it.
            ssh chooses $HOME but we aren't guaranteed that a home dir will
            exist in any given chroot.  So for now we're choosing "/" instead.
            This also happens to be the former default.

            Can revisit using $HOME instead if it's a problem
        '''
        if not remote_path.startswith(os.path.sep):
            remote_path = os.path.join(os.path.sep, remote_path)
        return os.path.normpath(remote_path)

    def put_file(self, in_path, out_path):
        """ Transfer a file from local to kubectl container """
        super(Connection, self).put_file(in_path, out_path)
        display.vvv("PUT %s TO %s" % (in_path, out_path), host=self._play_context.remote_addr)

        out_path = self._prefix_login_path(out_path)
        if not os.path.exists(to_bytes(in_path, errors='surrogate_or_strict')):
            raise AnsibleFileNotFound(
                "file or module does not exist: %s" % in_path)

        out_path = shlex_quote(out_path)
        # kubectl doesn't have native support for copying files into
        # running containers, so we use kubectl exec to implement this
        args = self._build_exec_cmd([self._play_context.executable, "-c", "dd of=%s bs=%s" % (out_path, BUFSIZE)])
        args = [to_bytes(i, errors='surrogate_or_strict') for i in args]
        with open(to_bytes(in_path, errors='surrogate_or_strict'), 'rb') as in_file:
            try:
                p = subprocess.Popen(args, stdin=in_file,
                                     stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            except OSError:
                raise AnsibleError("kubectl connection requires dd command in the container to put files")
            stdout, stderr = p.communicate()

            if p.returncode != 0:
                raise AnsibleError("failed to transfer file %s to %s:\n%s\n%s" % (in_path, out_path, stdout, stderr))

    def fetch_file(self, in_path, out_path):
        """ Fetch a file from container to local. """
        super(Connection, self).fetch_file(in_path, out_path)
        display.vvv("FETCH %s TO %s" % (in_path, out_path), host=self._play_context.remote_addr)

        in_path = self._prefix_login_path(in_path)
        out_dir = os.path.dirname(out_path)

        # kubectl doesn't have native support for fetching files from
        # running containers, so we use kubectl exec to implement this
        args = self._build_exec_cmd([self._play_context.executable, "-c", "dd if=%s bs=%s" % (in_path, BUFSIZE)])
        args = [to_bytes(i, errors='surrogate_or_strict') for i in args]
        actual_out_path = os.path.join(out_dir, os.path.basename(in_path))
        with open(to_bytes(actual_out_path, errors='surrogate_or_strict'), 'wb') as out_file:
            try:
                p = subprocess.Popen(args, stdin=subprocess.PIPE,
                                     stdout=out_file, stderr=subprocess.PIPE)
            except OSError:
                raise AnsibleError("kubectl connection requires dd command in the container to fetch files")
            stdout, stderr = p.communicate()

            if p.returncode != 0:
                raise AnsibleError("failed to fetch file %s to %s:\n%s\n%s" % (in_path, out_path, stdout, stderr))

        if actual_out_path != out_path:
            os.rename(to_bytes(actual_out_path, errors='strict'), to_bytes(out_path, errors='strict'))

    def close(self):
        """ Terminate the connection. Nothing to do for kubectl"""
        super(Connection, self).close()
        self._connected = False
