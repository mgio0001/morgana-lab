#!/usr/bin/env bash
#   Use this script to test if a given TCP host/port are available

cmd=/usr/bin/wait-for-it.sh
configdb=/opt/imail/config/config.db

if [ -f ${configdb} ]; then
  ${cmd} --host=dir01a --port=8761 --timeout=120
fi

/usr/sbin/init
