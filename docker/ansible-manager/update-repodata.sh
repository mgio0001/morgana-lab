#!/bin/bash

outputdir='/tmp/repodata-tmp/'
reporoot='/workspace/repo/rpms/'
tarroot='/workspace/repo/tarfiles/'

mkdir -p ${outputdir}/repodata

CROPTS="--outputdir=${outputdir}"

for i in ansible centos centos/7 centos/6 owm owm/9 owm/9/1 owm/9/2 owm/9/5; do
	echo "path: ${reporoot}${i}"
	if [ -d "${reporoot}${i}/repodata" ]; then
		echo "${reporoot}${i}/repodata exists"
		rm -rf "${reporoot}${i}/repodata"
	fi
	createrepo ${CROPTS} ${reporoot}${i}
	mv ${outputdir}/repodata ${reporoot}${i}/
done;

rm -rf ${outputdir}

# it makes files readable to httpd
find $reporoot -type f -exec chmod go+r {} \;
find $tarroot -type f -exec chmod go+r {} \;
find $reporoot -type f -exec chown root:root {} \;
find $tarroot -type f -exec chown root:root {} \;

# it makes directories executable to the httpd
find $reporoot -type d -exec chmod go+xr {} \;
find $tarroot -type d -exec chmod go+xr {} \;

# NB: it will be required to fix context on SELinux enabled systems
# restorecon -FRvv /opt/repo 

# This is to be sure all hosts will have their yum local cache updated
# '*-pb-*' limit the command to AUH staging
#cd /opt/merlin
#ansible -i staging -m shell -b -a 'yum makecache' '*-pb-*'
