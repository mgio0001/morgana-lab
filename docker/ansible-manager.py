#!/usr/bin/python

import docker
import os
import time
from subprocess import call
if __name__ == '__main__':
    import argparse

DEBUG = 0


class AnsibleManager:
    """Ansible Manager tool"""

    ANSIBLE_MANAGER_IMAGE_ID = 'ansible-manager'
    ANSIBLE_MANAGER_WORKSPACE_PATH = '/opt/morgana'
    DOCKER_SOCKET_PATH = '/var/run/docker.sock'
    DOCKER_COMPOSE_FILE_PATH = '{}/{}'.format(
        ANSIBLE_MANAGER_WORKSPACE_PATH,
        'docker/docker-compose.yml')
    DOCKER_COMPOSE_COMMAND = '/usr/bin/docker-compose'
    ANSIBLE_MANAGER_NETWORK_ID = 'labnet'
    ANSIBLE_MANAGER_NETWORK_CIR = '172.19.0.0/16'
    ANSIBLE_MANAGER_NETWORK_GW = '172.19.0.1'
    ANSIBLE_MANAGER_REPO_IMAGE_ID = 'repo'
    ANSIBLE_MANAGER_REPO_PATH = '/workspace'
    ANSIBLE_MANAGER_UPDATE_REPO_COMMAND = '/usr/bin/update-repodata.sh'

    MORGANA_CMDS = {
        'setup_common': 'morgana-01-common.sh',
        'setup_cassmeta': 'morgana-02-cassmeta.sh',
        'setup_qelasticsearch': 'morgana-04-qelasticsearch.sh',
        'setup_mx': 'morgana-05-mx.sh',
        'setup_ux': 'morgana-06-uxfe.sh',
        'setup_pss': 'morgana-20-provisioner.sh',
        'setup_dac': 'morgana-21-dac.sh',
        'setup_migration': 'morgana-30-hirundo.sh',
        'setup_healthstation': 'morgana-100-healthstation.sh'
    }

    MORGANA_SETS = {
        'setup_all': [
            'setup_common',
            'setup_cassmeta',
            'setup_qelasticsearch',
            'setup_mx',
            'setup_ux',
            'setup_pss',
            'setup_dac',
            'setup_migration',
            'setup_healthstation'
        ],
        'setup_mx_platform': [
            'setup_common',
            'setup_cassmeta',
            'setup_qelasticsearch',
            'setup_mx'
        ],
        'setup_pss_platform': [
            'setup_pss',
            'setup_dac'
        ]
    }

    CONTAINER_CMDS = {
        'containers_ps': 'ps',
        'containers_start': 'start',
        'containers_stop': 'stop',
        'containers_rm': 'rm -f',
        'containers_images': 'build',
        'containers_create': 'up -d'
    }

    def __init__(self):
        """Init.

        Create docker from env and check images.
        Init arguments.
        Check arguments.
        Executes.
        """
        if DEBUG > 0:
            print '__init__'
        self.docker = docker.from_env()
        self.__check_network()
        self.__init_args()
        self.__check_ansible_manager_images()
        self.__check_args()

    def __init_args(self):
        """Initializes arguments parser."""
        if DEBUG > 0:
            print '__init_args'
        if __name__ == '__main__':
            parser = argparse.ArgumentParser(
                prog=AnsibleManager.ANSIBLE_MANAGER_IMAGE_ID,
                formatter_class=argparse.RawDescriptionHelpFormatter,
                description="""AnsibleManager.
================================================================================
This tool supports all the docker-compose commands that are executed through an
ansible-enabled container that deployes the Mx platform through the
Merlin/Morgana plays and roles.

It supports 3 different actions:

Composed setup actions:
{0}

Single setup actions:
{1}

Containers management actions:
{2}

Direct access to the AnsibleManager container:
ansible-manager-run""".format(
                    AnsibleManager.MORGANA_SETS.keys(),
                    AnsibleManager.MORGANA_CMDS.keys(),
                    AnsibleManager.CONTAINER_CMDS.keys()
                )
            )
            parser.add_argument(
                '--workspace',
                help="""The local workspace to use
                (default '.') where ansible plays
                 and docker file are stored"""
            )
            parser.add_argument(
                '--repository',
                help="""The local repository to use
                (default './packages/') where repo/* packages are stored"""
            )
            parser.add_argument(
                '--dockerfiles',
                help="""The local repository to use
                (default './docker/') where docker projects are stored"""
            )
            parser.add_argument(
                'action',
                nargs='*',
                help="Composed, single or containers actions",
                choices=['updaterepo', 'ansible-manager-run'] +
                AnsibleManager.MORGANA_SETS.keys() +
                AnsibleManager.MORGANA_CMDS.keys() +
                AnsibleManager.CONTAINER_CMDS.keys()
            )
            self.__args = parser.parse_args()
            if self.__args.workspace:
                self.__workspace_path = self.__args.workspace
            else:
                self.__workspace_path = os.path.abspath(os.path.curdir)
            self._repository_path = '{0}/packages'.format(
                os.path.abspath(os.path.curdir)
            )
            self._dockerfile_path = '{0}/docker'.format(
                os.path.abspath(os.path.curdir)
            )
            if self.__args.repository:
                self._repository_path = self.__args.repository
            if self.__args.dockerfiles:
                self._dockerfile_path = self.__args.dockerfiles

    def __check_args(self):
        """Check provided arguments from the command line and executes
        according."""
        if DEBUG > 0:
            print '__check_args'
        print self.__args.action
        if set(self.__args.action).intersection(
            AnsibleManager.CONTAINER_CMDS.keys()
        ).__len__() > 0:
            self.__compose(self.__args.action)
        if set(self.__args.action).intersection(
            AnsibleManager.MORGANA_CMDS.keys()
        ).__len__() > 0:
            self.__morgana(self.__args.action)
        if set(self.__args.action).intersection(
            AnsibleManager.MORGANA_SETS.keys()
        ).__len__() > 0:
            self.__morgana_sets(self.__args.action)
        if set(self.__args.action).intersection(['updaterepo']):
            self.__updaterepo()
        if set(self.__args.action).intersection(['ansible-manager-run']):
            self.__ansible_manager_access()

    def __check_network(self):
        """Check required network is configured"""
        if DEBUG > 0:
            print '__check_network'
        try:
            self.docker.networks.get(
                network_id=AnsibleManager.ANSIBLE_MANAGER_NETWORK_ID)
        except docker.errors.NotFound as e:
            print 'Network {0} not found (error{1})'.format(
                AnsibleManager.ANSIBLE_MANAGER_NETWORK_ID,
                e
            )
            print 'Creating network {}...'.format(
                AnsibleManager.ANSIBLE_MANAGER_NETWORK_ID)
            self.docker.networks.create(
                name=AnsibleManager.ANSIBLE_MANAGER_NETWORK_ID,
                ipam=docker.types.IPAMConfig(
                    pool_configs=[docker.types.IPAMPool(
                        subnet=AnsibleManager.ANSIBLE_MANAGER_NETWORK_CIR,
                        gateway=AnsibleManager.ANSIBLE_MANAGER_NETWORK_GW
                    )]
                ),
                attachable=True
            )

    def __ansible_manager_access(self):
        self.__check_ansible_manager_repo_images()
        self.__ansible_manager_repo_run()
        call([
            'docker',
            'run',
            '-it',
            '--rm',
            '--network',
            AnsibleManager.ANSIBLE_MANAGER_NETWORK_ID,
            '--hostname',
            AnsibleManager.ANSIBLE_MANAGER_IMAGE_ID,
            '-v',
            '{}:/opt/morgana'.format(os.path.abspath(os.path.curdir)),
            '-v',
            '/var/run/docker.sock:/var/run/docker.sock',
            AnsibleManager.ANSIBLE_MANAGER_IMAGE_ID,
            '/usr/bin/bash-access.sh'
        ])

    def __check_ansible_manager_images(self):
        """Check required images are present."""
        if DEBUG > 0:
            print '__check_ansible_manager_images'
        try:
            print "Check AnsibleManager images.."
            self.docker.images.get(AnsibleManager.ANSIBLE_MANAGER_IMAGE_ID)
            print "Done."
        except docker.errors.ImageNotFound as e:
            print 'AnsibleManager image not available: {}'.format(e)
            print 'Create image {}...'.format(
                AnsibleManager.ANSIBLE_MANAGER_IMAGE_ID)
            self.docker.images.build(
                path='{0}/{1}'.format(
                    self._dockerfile_path,
                    AnsibleManager.ANSIBLE_MANAGER_IMAGE_ID
                ),
                tag=AnsibleManager.ANSIBLE_MANAGER_IMAGE_ID
            )
            print('...Done.')

    def __check_ansible_manager_repo_images(self):
        """Check required images are present."""
        if DEBUG > 0:
            print '__check_ansible_manager_repo_images'
        try:
            print "Check AnsibleManager Repository images.."
            self.docker.images.get(
                AnsibleManager.ANSIBLE_MANAGER_REPO_IMAGE_ID)
            print "Done."
        except docker.errors.ImageNotFound as e:
            print 'AnsibleManager Repository image not available: {}'.format(e)
            print 'Create image {}...'.format(
                AnsibleManager.ANSIBLE_MANAGER_REPO_IMAGE_ID)
            self.docker.images.build(
                path='{0}/{1}'.format(
                    self._dockerfile_path,
                    AnsibleManager.ANSIBLE_MANAGER_REPO_IMAGE_ID
                ),
                tag=AnsibleManager.ANSIBLE_MANAGER_REPO_IMAGE_ID
            )
            print('...Done.')

    def __updaterepo(self):
        """Update repos"""
        if DEBUG > 0:
            print '__updaterepo'
        res = self.__ansible_manager_run(
            [AnsibleManager.ANSIBLE_MANAGER_UPDATE_REPO_COMMAND])
        for line in res.attach(stream=True):
            print str(line).strip()
        res.wait()

    def __ansible_manager_repo_run(self):
        """Execute the httpd based container servicing packages"""
        if DEBUG > 0:
            print '__ansible_manager_repo_run'
        try:
            c = self.docker.containers.get(
                AnsibleManager.ANSIBLE_MANAGER_REPO_IMAGE_ID)
            c.kill()
            c.wait()
            print 'Ansible-Manager: repo still running has been stopped'
            for i in range(5):
                self.docker.containers.get(
                    AnsibleManager.ANSIBLE_MANAGER_REPO_IMAGE_ID)
                time.sleep(5)
        except docker.errors.DockerException:
            print 'Ansible-Manager: repo check complete'
        self.docker.containers.run(
            image=AnsibleManager.ANSIBLE_MANAGER_REPO_IMAGE_ID,
            hostname=AnsibleManager.ANSIBLE_MANAGER_REPO_IMAGE_ID,
            name=AnsibleManager.ANSIBLE_MANAGER_REPO_IMAGE_ID,
            remove=True,
            detach=True,
            network=AnsibleManager.ANSIBLE_MANAGER_NETWORK_ID,
            volumes={
                self._repository_path: {
                    'bind': AnsibleManager.ANSIBLE_MANAGER_REPO_PATH
                }
            },
            command=None
        )

    def __ansible_manager_repo_stop(self):
        """Stops running httpd repo"""
        print "Ansible-Manager: Stop repo"
        res = self.docker.containers.get(
            AnsibleManager.ANSIBLE_MANAGER_REPO_IMAGE_ID)
        res.stop()
        res.wait()

    def __ansible_manager_run(self, cmd):
        """Execute docker run equivalent commands through the ansible-manager
        container."""
        if DEBUG > 0:
            print '__ansible_manager_run'
        try:
            c = self.docker.containers.get(
                AnsibleManager.ANSIBLE_MANAGER_IMAGE_ID)
            c.kill()
            c.wait()
            print """Ansible-Manager: ansible-manager still
            running has been stopped"""
            for i in range(5):
                self.docker.containers.get(
                    AnsibleManager.ANSIBLE_MANAGER_IMAGE_ID)
                time.sleep(5)
        except docker.errors.DockerException:
            print "Ansible-Manager: ansible-manager check complete"
        if isinstance(cmd, str):
            cmd = [cmd]
        print 'Ansible-Manager CMD: {}'.format(cmd)
        return self.docker.containers.run(
            image=AnsibleManager.ANSIBLE_MANAGER_IMAGE_ID,
            hostname=AnsibleManager.ANSIBLE_MANAGER_IMAGE_ID,
            name=AnsibleManager.ANSIBLE_MANAGER_IMAGE_ID,
            remove=True,
            auto_remove=True,
            detach=True,
            volumes={
                AnsibleManager.DOCKER_SOCKET_PATH: {
                    'bind': AnsibleManager.DOCKER_SOCKET_PATH
                },
                self.__workspace_path: {
                    'bind': AnsibleManager.ANSIBLE_MANAGER_WORKSPACE_PATH
                },
                self._repository_path: {
                    'bind': AnsibleManager.ANSIBLE_MANAGER_REPO_PATH
                }
            },
            command=' '.join(cmd)
        )

    def __morgana_sets(self, cmd):
        """Execute morgana set scripts"""
        if DEBUG > 0:
            print '__morgana_sets'
        for c in cmd:
            for cc in AnsibleManager.MORGANA_SETS[c]:
                print "running " + cc
                self.__morgana([cc])

    def __morgana(self, cmd):
        """Execute morgana scripts."""
        if DEBUG > 0:
            print '__morgana'
#        _cmd = cmd
#        if 'setup_all' in cmd:
#            _cmd = AnsibleManager.MORGANA_CMDS.keys()
#            _cmd.remove('setup_all')
        self.__check_ansible_manager_repo_images()
        self.__ansible_manager_repo_run()
        for c in cmd:
            print "Morgana deployment: " + c
            res = self.__ansible_manager_run(
                '{0}/{1} {2} {3}'.format(
                    AnsibleManager.ANSIBLE_MANAGER_WORKSPACE_PATH,
                    AnsibleManager.MORGANA_CMDS[c],
                    '--inventory',
                    '/opt/morgana/morgana-lab'
                )
            )
            for line in res.attach(stream=True):
                print str(line).strip()
            code = res.wait()
            if code != 0:
                print 'Return code: {}'.format(code)
                exit()

        self.__ansible_manager_repo_stop()

    def __compose(self, cmd):
        """Execute docker-compose commands."""
        if DEBUG > 0:
            print '__compose'
        _cmd = [
            AnsibleManager.DOCKER_COMPOSE_COMMAND,
            '-f',
            AnsibleManager.DOCKER_COMPOSE_FILE_PATH,
            AnsibleManager.CONTAINER_CMDS[' '.join(cmd)]
        ]
        res = self.__ansible_manager_run(_cmd)
        for line in res.attach(stream=True):
            print str(line).strip()
        res.wait()


if __name__ == '__main__':
    try:
        AnsibleManager()
    except KeyboardInterrupt:
        print "exit"
