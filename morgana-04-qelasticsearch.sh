#!/bin/bash

# this script execute the deployment of the ELK cluster
#
# author: yari.latini@synchronoss.com


# REINSTALL PROCEDURE
#
# in order to reset the changes from this playbook, execute the command below:
# kill the cassandra processes
#
# ansible -b -m shell -a 'systemctl stop elasticsearch' <target_host> -i <inventory-file>
#
# remove data and package
# ansible -b -m shell -a 'yum remove -y elasticsearch' <target_host> -i <inventory-file>
# ansible -b -m shell -a 'rm -rf /opt/data/elasticsearch' <target_host> -i <inventory-file>

# different playbook could have different tags from Merlin you may want to skip
# set below the comma separated list of tags you want to ignore
TAGBL="--skip-tags=owm-common,hostname,timezone,userpermissions,users,ntp,dns,hosts"
# this is the playbook you will execute
PLAYBOOK="morgana-qelasticsearch.yml"

# parse options and execute
source $(dirname $0)/lib.sh

# Useful mss meta commands
#
# MANAGEMENT =====================================================
#
# Update the configuration files
# ./prod01-elasticsearch.sh --tags owm-elk-init,owm-elk-confupdates
#
# START / STOP SERVICES ================================================
#
# Restart the instances
# ansible -b -m shell -a 'systemctl restart elasticsearch' elk-search -i <inventory-file>
#
# MONITORING COMMANDS ==================================================
#
# ref: https://www.elastic.co/guide/en/elasticsearch/guide/current/cluster-admin.html
# ref: https://www.elastic.co/guide/en/elasticsearch/guide/current/_cat_api.html
#
# Check cluster status (green -> ok, yellow -> warning, red -> troubles)
# ansible -m shell -a 'curl -XGET http://localhost:9200/_cat/health?v 2>/dev/null' <elk-node> -i <inventory-file>
#
# Check indices status (if status != green)
# ansible -i <inventory-file> -m shell -a 'curl -XGET http://localhost:9200/_cluster/health?pretty=true\&level=indices 2>/dev/null' <elk-node>
# or
# ansible -i <inventory-file> -m shell -a 'curl -XGET http://localhost:9200/_cat/indices?v 2>/dev/null' <elk-node>
#
# Check shards status
# ansible -i <inventory-file> -m shell -a 'curl -XGET http://localhost:9200/_cluster/health?pretty=true\&level=shards 2>/dev/null' <elk-node>
# or
# ansible -i <inventory-file> -m shell -a 'curl -XGET http://localhost:9200/_cat/shards?v 2>/dev/null' <elk-node>
#
# Check disk usage
# ansible -i <inventory-file> -m shell -a '{% if elasticsearch_data_path is defined %}df -h {{elasticsearch_data_path}}{% else %}echo "No data"{% endif %}' elk-search
#
# Check nodes stats (https://www.elastic.co/guide/en/elasticsearch/guide/current/_monitoring_individual_nodes.html)
# ansible -i <inventory-file> -m shell -a 'curl -XGET http://localhost:9200/_nodes/stats?pretty=true 2>/dev/null' <elk-node>
# or
# ansible -i <inventory-file> -m shell -a 'curl -XGET http://localhost:9200/_cat/nodes?v 2>/dev/null' <elk-node>
#
# Check cluster stats (https://www.elastic.co/guide/en/elasticsearch/guide/current/_cluster_stats.html)
# ansible -i <inventory-file> -m shell -a 'curl -XGET http://localhost:9200/_cluster/stats?pretty=true 2>/dev/null' <elk-node>
#
# Check index stats (https://www.elastic.co/guide/en/elasticsearch/guide/current/_index_stats.html)
# ansible -i <inventory-file> -m shell -a 'curl -XGET http://localhost:9200/_all/_stats?pretty=true 2>/dev/null' <elk-node>
#
# Check pending tasks (https://www.elastic.co/guide/en/elasticsearch/guide/current/_pending_tasks.html)
# ansible -i <inventory-file> -m shell -a 'curl -XGET http://localhost:9200/_cluster/pending_tasks?pretty=true 2>/dev/null' <elk-node>
#
# CONFIGURATION CHECKS =================================================
#
# Check configuration
# ansible -i <inventory-file> -b -m shell -a 'egrep -v "^#|^$" /etc/elasticsearch/elasticsearch.yml' elk-search
#
