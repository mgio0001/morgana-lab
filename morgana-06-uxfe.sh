#!/bin/bash

# this script execute the deployment of the pab and cal
#
# author: yari.latini@synchronoss.com

# Before this script, please execute:
# 1) prod00-common.sh
# 2) prod01-confserv.sh
# 3) prod02-dirserv.sh
# 4) prod02-sdiscovery.sh
# 5) prod03-mos.sh
# 6) prod03-mss.sh
# 7) prod04-qservice.sh
# 8) prod05-affinity.sh

# Applies or updates basic configuration on the managed hosts including:
# 1) execute common tasks (same as prod00-common)
# 2) install owm-common, owm-mgs-common, owm-pab and owm-cal

# REINSTALL PROCEDURE
#
# in order to reset the changes from this playbook, execute the command below (stop primary as last if you're reinstalling everything):
#
# ansible -i <inventory-file> -b -f1 -m shell -a 'systemctl stop mx' <target_host>
#
# remove UI
# ansible -i morgana-lab -b -m shell -a 'rm -rf /opt/imail/webtop/webapps/eux' <target-host>
# ansible -i morgana-lab -b -m shell -a 'rm -rf /opt/imail/webtop/webapps/webtop-media/WEB-INF/logs' <target-host>
#
# remove rpms 
# ansible -i morgana-lab -b -m shell -a 'yum remove -y owm-webtop-tomcat-manager owm-webtop-tomcat-host-manager owm-webtop-tomcat owm-common owm-webtop-media openoffice*' <target-host>
#

# different playbook could have different tags from Merlin you may want to skip
# set below the comma separated list of tags you want to ignore
TAGBL="--skip-tags=owm-webtop-rpmforge,owm-webtop-webmail,hostname,timezone,userpermissions,users,ntp,dns,limits,hosts"
# this is the playbook you will execute
PLAYBOOK="morgana-ux.yml"

# parse options and execute
source $(dirname $0)/lib.sh

# Useful commands
#
# START / STOP SERVICES ================================================
#
# Restart the instances (important -f1, stop primary as last and start primary as first)
# ansible -i <inventory-file> -b -f1 -m shell -a 'systemctl stop mx' <target_host>
# ansible -i morgana-lab -b -f1 -m shell -a 'runuser -l imail -c "/opt/imail/lib/imservctrl restart webtop"' <targethost>
# ansible -i morgana-lab -b -f1 -m shell -a 'runuser -l imail -c "/opt/imail/lib/imservctrl stop webtop"' <targethost>
# ansible -i morgana-lab -b -f1 -m shell -a 'runuser -l imail -c "/opt/imail/lib/imservctrl start webtop"' <targethost>
#
# CONFIGURATION CHANGES ================================================
#
# Deployment of a new UX
# ./prod06-uxfe.sh --tags owm-ux,owm-ux-init --target <host-id>
#
# MONITORING COMMANDS ==================================================
#
# Check process are up
# ansible -i morgana-lab -b -f1 -m shell -a 'runuser -l imail -c imservping' <target-host>
# 
# Check OpenOffice is running
# ansible -i morgana-lab -b -f1 -m shell -a 'pgrep -l soffice.bin' <target-host>
#
# Check config.db alignment
# ansible -i morgana-lab -b -f1 -m shell -a 'runuser -l imail -c "md5sum config/config.db"' <target-host> -o
#
# Check processes (imconfserv and immgrsev MUST be up and running)
# ansible -i morgana-lab -b -f1 -m shell -a 'runuser -l imail -c imservping' <target-host>
#
# Check current log files for Errors/Warning/Critical messages
# ansible -i morgana-lab -b -f1 -m shell -a 'runuser -l imail -c "find log/ -name \*.log -type l | xargs grep -v Note | imlogprint -m"' mss
#
# CHECK MAINTENANCE ====================================================
#
# CONFIGURATION CHECKS =================================================
#
