/*
 * Copyright (c) 2014 Openwave Messaging Systems Inc.
 * All Rights Reserved.
 *
 * This software is the proprietary information of Openwave Messaging Systems Inc.
 * Use is subject to license terms.
 *
 */

/**
 *  Custom webtop configs overwrites global WebtopConfig object.
 *  Please refer to client-config.js for more details.
 */
var CustomWebtopConfig = {
    defaultSkin: 'default',
    // The suffix for IE skin, for kiwi/master it should '' , because use same images for all device.
    IESkinSuffix: '',
    cloud: {
        enabled: false,
        defaultUploadFolder: 'auto-upload',
        defaultUploadFolderId: undefined,
        cdmiAttachmentSize: 600 // should cover long file names too
    },
    localeSettings: {
        defaultLocale: 'en_US',
        localeDir: 'resources/locale/',

        locales: [
            {
                name : 'en_US',
                title: 'English',
                files: 'sdk/en_US.js'
            },
            {
                name : 'zh_CN',
                title: '中文 (简体)',
                files: 'sdk/zh_CN.js'
            },
            {
                name : 'zh_TW',
                title: '中文 (繁體)',
                files: 'sdk/zh_TW.js'
            },
            {
                name : 'ja_JP',
                title: '日本語',
                files: 'sdk/ja_JP.js'
            },
            {
                name : 'it_IT',
                title: 'Italiano',
                files: 'sdk/it_IT.js'
            },
            {
                name : 'fr_FR',
                title: 'Français',
                files: 'sdk/fr_FR.js'
            },
            {
                name : 'de_DE',
                title: 'Deutsch',
                files: 'sdk/de_DE.js'
            },
            {
                name : 'es_ES',
                title: 'Español',
                files: 'sdk/es_ES.js'
            },
            {
                name: 'ar_AE',
                title: 'العربية (UAE)',
                files: 'sdk/ar_AE.js',
                direction: 'rtl',
                extendsFramework: true
            },
            {
                name : 'en_RTL',
                title: 'English (RTL)',
                files: 'sdk/en_US.js',
                direction: 'rtl',
                extendsFramework: true
            }
        ],

        extLocaleMap: {
            en_US: 'en',
            it_IT: 'it',
            ja_JP: 'ja',
            fr_FR: 'fr',
            de_DE: 'de',
            es_ES: 'es'
        }
    },

    user: {
        defaultUserDomain: 'rwc-hinoki05.owmessaging.com',
        supportCsrfProtection: true
    },

    framework: {
        enableInternalAccount: false
    },

    contacts: {
        enablecontactgroups: true,
        searchFields: 'firstName,lastName,lzEmail,lzPhone'
    },

    mail: {
        layoutPriority: {
            previewRight: true,
            previewBelow: true,
            noPreview: true,
            attachmentView: true,
            conversationView: true
        },
        enableHeaderIcon: true,
        enableSizeColumn: false,
        enableGlobalSearch: true,
        whiteListing: false
    },
    settings: {
        preferences: {
            ConversationEnable: { key: 'conversationEnable', enable: true, defaultValue: 'false'}
        }
    },

    /**
     * UI customize configuration.
    */
    ui: {
        /**
        * RichUI configuration.
         */
        rui: {
            viewport: {
                minWidth: 1024, minHeight: 600
            },
            enableMultiTab:false,
            /**
             * Layout configuration
             */
            layout: {
                minWidth: 1024,
                toolbarHeight: 52,
                appbarHeight: 36,
                navigateBar: {
                    width: '100%',
                    height: 436
                },
                navigateButton: {
                    height: 32,
                    padding: '0 10'
                },
                navigateAux: {
                    height: 36
                },
                categoryPanel: {
                    minWidth: 200,
                    width: 200
                },
                listPanel: {
                    minWidth: 350
                },
                detailPanel: {
                    minWidth: 554
                },
                previewPanel: {
                    padding: 0
                },
                mainToolbar: {
                    padding: '0 10 0 0'
                },
                accountContainer: {
                    mailPlaceHolder: {
                        width: 40
                    }
                }
            },
            navigation: {
                settings: {
                    movedToAux: true
                }
            },
            mail: {
                toolbarButton:  { refresh: 'icon' },
                columns: {
                    seen: 45,
                    replyOrForward: 30,
                    conversation: 30,
                    flagged: 30,
                    attachment: 30,
                    priority: 85,
                    size: 100,
                    hidePriority: false
                }
            },
            help: {
                url:{
                    help: {
                        "en_US": "help/en_US/RichUI/Default.htm",
                        "es_ES": "help/es_ES/RichUI/Default.htm"
                    },
                    accessibility: 'help/en_US/AccessUI/Default.htm'
                }
            },
            calendar: {
                listToolbarButtonConfig:  { refreshBtn: 'icon' }
            },
            task: {
                tasklist: {
                    toolbar:{
                        showSortBtnButtonMode: 'icon'
                    }
                }
            },
            settings: {
                noAuthority: {
                    profile: false,
                    vCard: false,
                    languageTimeDate: false,
                    password: false,
        //          mail: false, should always show
                    accounts: false,
                    signature: false,
                    blockedSenders: false,
                    safeSenders: false,
                    rules: false,
                    autoForward: false,
                    autoReply: false,
                    disposableAddress: false,
                    blockImage: false,
                    calendar: false
                },
                toolbar: {
                    btnMode: 'both'
                },
                forms: {
                    btnMode: 'both'
                }
            }
        },
        tui: {
            footerLinks: [
                {
                    "href": "http://www.cp.net/emailmanagement",
                    "text": "MANAGE_YOUR_MAIL"
                },
                {
                    "href": "http://www.cp.net/contact",
                    "text": "CONTACT_US"
                },
                {
                    "href": "http://www.cp.net/feedback",
                    "text": "SEND_US_FEEDBACK"
                },
                {
                    "href": "http://www.cp.net/privacy-policy",
                    "text": "PRIVACY_POLICY"
                },
                {
                    "href": "http://www.cp.net/terms-and-conditions",
                    "text": "TERMS_CONDITIONS"
                },
                {
                    href: {
                        "en_US": "help/en_US/TouchUI/Default.htm",
                        "es_ES": "help/es_ES/TouchUI/Default.htm"
                    },
                    "text": "HELP"
                }
            ]

        }
    },

    globalHeaderUrl: {
        rui: 'header/header.html'
    }
};

/**
 * Custom contants which will be applied to global Constant namespace.
 */
var CustomConstant = {
    // This will overwrite Constant.AD_BANNER_URL
    // AD_BANNER_URL: 'ad/ad_320_50.html'
};

/**
 *  Overlaying custom config on top of WebtopConfig
 */
if (window.mergeObj && window.WebtopConfig) {
    WebtopConfig = mergeObj(WebtopConfig, CustomWebtopConfig);
}
