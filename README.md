# Morgana #

Morgana is a project built on top of [Merlin](https://bitbucket.org/openwave/merlin).
Its main objective is to make access to Merlin feature easier for who has no experience with Ansible.
Morgana is currently focusing on Mx 9.2(.1.1) deployment, its roles are merged from merlin develop branch with some additional fixes and improvements that by time to time are pushed back to merlin.

A Wiki site is in progress with the purpose of create the required documentation. See [Morgana Wiki](https://bitbucket.org/openwave/morgana/wiki/Home)

### How to setup a management node ###

The wiki will provide all the information in order to setup the management node. See [Management node setup](https://bitbucket.org/openwave/morgana/wiki/Management%20node%20setup.)

### Known Issues ###

It has been noticed SUR masters deployments fail because SUR entries disapper from the config.db. For Etisalat tmp/imdirs-setup-workaround.txt provides the list of the entries to rescue after the deployment. It seems more likely a problem in the post-install scripts of the SUR package and a ticket has been raised https://jira.owmessaging.com/browse/SERVICES-4979

### Branches ###

branch | description
--- | ---
**master** | this is where we push and work. groups, hosts and inventory on this branch are for a small local LAB you can use for testing / development purpose (see [https://bitbucket.org/openwave/morgana/wiki/Home](Link URL) "5 minutes setup").
**releases** | this is where safe code go. If you're not sure and want to test morgana/merlin point to this branch.
**master-mx9.2** | this is the branch focusing on Mx 9.2 deployment (currently aligned with the master)
**master-mx9.5** | this is the branch focusing on the incoming Mx 9.5, merged with the incoming Merlin Mx 9.5. Once Mx 9.5 will reach GA state this will become the master of the morgana project
**openmind-lab** | this is the QA LAB we're using (we're focusing on Etisalat deployment, but this is quite generic and you can check it out for other project based on Mx 9.2). Details in order to access that environment: [https://village.owmessaging.com/display/PS/SNCR-ET+Quality+Assurance+LAB](Link URL)
**etisalat** | this is for the Etisalat deployment. Customer specific changes, hosts, groups and inventory data. Roles are supposed to be 100% aligned with release branch.
**etisalat_lab** | etisalat lab, based on openmind-lab, this is a good source for a LAB for the SNCR openstack environment
**merlin_develop** | this is the branch tracking merlin/develop branch.
**docker-lab** | this is the branch for a small docker lab, if you're familiar with docker you can setup a virtual host (8vCPU/32GB) and stack the whole Mx 9.5 with docker. It requires advanced docker knowledge since Ansible leverage on the docker connection plugin rather than the SSH one. Contact yari.latini@synchronoss.com for further details on the setup
**kubernetes-lab** | this is an experimental branch used for UAT on a kubernetes LAB replicating a multi-DC deployment, it is required to set up a kubernetes environment with at least 4-5 nodes (16vCPU/64GB) and have advanced knowledge on container technologies (and kubernetes in the specific). Contact yari.latini@synchronoss.com for further details on the setup
**docker-swarm-lab** | this is an experimental branch used to evaluate docker swarm within a wide Mx deployment. Currently on hold.

### TODO ###

* Improve documentation (always), please feel free to test and update the wiki
* Work in order to use ansible-galaxy for proper Merlin/Morgana based deployment rather than checkout / update the whole project
* Add automated non-functional tests at the end of every morgana-* group of tasks to speed up the deployment.
* Switch master-mx9.5 to master as soon as Mx 9.5 will be GA
* During UAT for Mx 9.5 it has been noticed the existing failover strategy on SUR failure doesn't work properly (see https://jira.synchronoss.net:8443/jira/browse/MX-6308). It could lead to changed to the current MxOS/SUR/HA configuration strategy, stay tuned...
* owm-backup and owm-restore must be updated as at the moment can't be used as they are.

### Contribution guidelines ###

* While using morgana (and Merlin) PSO will find bugs and way to improve or unreach the project. Checkout the project, make your test, apply your changes and push back them with a pull request. This is a project we want to build from the field and make available to everyone.
