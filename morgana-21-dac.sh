#!/bin/bash

# this script execute the deployment of the DAC
#
# author: yari.latini@synchronoss.com

# Before this script, please execute:
# 1) prod00-common.sh
# 2) prod01-confserv.sh
# 3) prod02-dirserv.sh
# 4) prod02-sdiscovery.sh
# 5) prod03-mos.sh
# 6) prod03-mss.sh
# 7) prod04-qservice.sh
# 8) prod05-affinity.sh
# 9) prod06-pabcal.sh
# 10) prod06-provisioner.sh

# REINSTALL PROCEDURE
#
# in order to reset the changes from this playbook, execute the command below (stop primary as last if you're reinstalling everything):
#
# ansible -i <inventory-file> -b -f1 -m shell -a 'systemctl stop dac' <target_host>
#
# remove rpms 
# ansible -i <inventory-file> -b -m shell -a 'rm -rf /opt/imail/DAC' <target_host>

# different playbook could have different tags from Merlin you may want to skip
# set below the comma separated list of tags you want to ignore
TAGBL="--skip-tags=hostname,timezone,userpermissions,users,ntp,dns,limits,hosts"
# this is the playbook you will execute
PLAYBOOK="morgana-dac.yml"

# parse options and execute
source $(dirname $0)/lib.sh

# Useful commands
#
# START / STOP SERVICES ================================================
#
# Restart the instances
# ansible -i <inventory-file> -b -f1 -m shell -a 'systemctl restart dac' <target_host>
#
# MONITORING COMMANDS ==================================================
#
# ansible -i <inventory-file> -b -f1 -m shell -a 'runuser -l imail -c "/opt/imail/provREST/provREST/provREST.sh status"' <target-host>
#
# API tests IN PROGRESS
# Authenticate
# ansible -i <inventory-file> -b -f1 -m shell -a 'curl -c /tmp/`echo .$$`  -b /tmp/`echo .$$` -v -X POST http://{{inventory_hostname}}:18680/prov/authenticate -d "user=superadmin@etisalat.int&password=password"' provisioner
# 
# Domain
# ansible -i <inventory-file> -b -f1 -m shell -a 'curl -c /tmp/`echo .$$`  -b /tmp/`echo .$$` -v -X GET http://{{inventory_hostname}}:18680/prov/domain/etisalat.int' provisioner

#FULL COMMANDS (to execute locally)
#
#PSS:
#
#Authenticate;
#curl -c /tmp/`echo .$$`  -b /tmp/`echo .$$` -v -X POST http://172.26.202.12:18680/prov/authenticate -d "user=admin@owmessaging.com&password=password"
#
#Domain cmd:
#curl -c /tmp/`echo .$$`  -b /tmp/`echo .$$` -v -X GET http://172.26.202.12:18680/prov/domain/owmessaging.com
#
#Admin cmd:
#curl -c /tmp/`echo .$$`  -b /tmp/`echo .$$` curl -v -X POST http://172.26.202.12:18680/prov/domain/mx.internal/user/admin -d "password=password&cosid=default&adminrole=ProfileDomainAdmin&adminObject=mx.internal"
#
#User cmd:
#
#curl -c /tmp/`echo .$$`  -b /tmp/`echo .$$` curl -v -X GET http://172.26.202.12:18680/prov/domain/mx.internal/user/testX 
#curl -c /tmp/`echo .$$`  -b /tmp/`echo .$$` curl -v -X POST http://172.26.202.12:18680/prov/domain/mx.internal/user/testX -d "password=password&cosid=default"
#

