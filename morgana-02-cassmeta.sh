#!/bin/bash

# this script execute the deployment of the mss meta rings
#
# author: yari.latini@synchronoss.com

# Before this script, please execute prod00-common.sh
# e.g.
# ./prod00-common.sh --target cassmetamss

# Applies or updates basic configuration on the managed hosts including:
# 1) execute common tasks (same as prod00-common)
# 2) install and configure cassandra meta rings for mss

# REINSTALL PROCEDURE
#
# in order to reset the changes from this playbook, execute the command below:
# kill the cassandra processes
#
# ansible -i <inventory-file> -b -m shell -a 'systemctl stop cassmeta' -b <target_host>
#
# remove data and accounts 
# ansible -i <inventory-file> -b -m user -a 'name=cassmeta state=absent remove=yes' -b <target_host>
# ansible -i <inventory-file> -b -m shell -a 'rm -rf /opt/metadata/{commitlog,data,saved_caches}' -b <target_host>

# different playbook could have different tags from Merlin you may want to skip
# set below the comma separated list of tags you want to ignore
TAGBL="--skip-tags=owm-queue-service,hostname,timezone,userpermissions,users,ntp,dns,limits,hosts"
# this is the playbook you will execute
PLAYBOOK="morgana-cassmeta.yml"

# parse options and execute
source $(dirname $0)/lib.sh

# Useful mss meta commands
#
# TEST MAINTENANCE =====================================================
#
# Execute the maintenance scripts node by node (recommended after the deployment in order to test the nodes and their maintenance)
# ansible -i <inventory-file> -b -m shell -a 'bash {{cass_user[0].home}}/scripts/cassmeta_maintenance.sh' cassmetamss -f1
#
# Check cronjobs have been installed and scheduled properly (very weekday are well distributed among nodes and servers)
# ansible -i <inventory-file> -b -m shell -a 'cat /var/spool/cron/{{cass_user[0].username}}' cassmetamss -f1
#
# Update Configuration if needed =======================================
#
# Update only the maintenance scripts
# ./prod01-mssmetacass.sh --tags maintenance,owm-cassmeta-init
#
# Update the configuration files
# ./prod01-mssmetacass.sh --tags owm-cassmeta-init,owm-cassmeta-confupdates
#
# START / STOP SERVICES ================================================
#
# Restart the instances
# ansible -i <inventory-file> -b -m shell -a 'systemctl restart cassmeta' cassmetamss
#
# Execute nodetool <command>
# ansible -i <inventory-file> -b -m shell -a 'runuser -l {{cass_user[0].username}} -c "cassandra-meta/cassandra/bin/nodetool <command>"' cassmetamss
#
# MONITORING COMMANDS ==================================================
#
# Check ring status
# ansible -i <inventory-file> -b -m shell -a 'runuser -l {{cass_user[0].username}} -c "cassandra-meta/cassandra/bin/nodetool ring KeyspaceMetadata"' cassandra1-hrc
#
# Check compaction status
# ansible -i <inventory-file> -b -m shell -a 'runuser -l {{cass_user[0].username}} -c "cassandra-meta/cassandra/bin/nodetool compactionstats"' cassmetamss
#
# Check thread pool statistics
# ansible -i <inventory-file> -b -m shell -a 'runuser -l {{cass_user[0].username}} -c "cassandra-meta/cassandra/bin/nodetool tpstats"' cassmetamss
#
# Check snapshots
# ansible -i <inventory-file> -b -m shell -a 'runuser -l {{cass_user[0].username}} -c "cassandra-meta/cassandra/bin/nodetool listsnapshots"' cassmetamss
# ansible -i <inventory-file> -b -m shell -a '{% for m in data_file_directories %} find  {{ m }} -name snapshots -type d {% endfor %}' cassmetamss
#
# Check disk usage
# ansible -i <inventory-file> -b -m shell -a '{% for m in data_file_directories %} df -h {{ m }} {% endfor %}' cassmetamss
#
# Check log files
# ansible -i <inventory-file> -b -m shell -a 'runuser -l {{cass_user[0].username}} -c "egrep -v \"INFO|^(\s+)|^java\.\" cassandra-meta/log/system.log"' cassmetamss
#
# CONFIGURATION CHECKS =================================================
#
# Check for specific configuration parameters (e.g. seeds)
# ansible -i <inventory-file> -b -m shell -a 'runuser -l {{cass_user[0].username}} -c "grep seeds cassandra-meta/cassandra/conf/cassandra.yaml"' cassmetamss
#
# Check cassandra-topology.properties
# ansible -i <inventory-file> -m shell -a 'grep -v '^#' {{cass_user[0].home}}/cassandra-meta/cassandra/conf/cassandra-topology.properties' cassmetamss -b
#
# Check Keyspace (or run any other cqlsh command)
# ansible -i <inventory-file> -b -m shell -a 'runuser -l {{cass_user[0].username}} -c "cassandra-meta/cassandra/bin/cqlsh {{inventory_hostname}} 9042 -k KeyspaceMetadata -e DESCRIBE\ KEYSPACE"' <cassandra-blob-node>
