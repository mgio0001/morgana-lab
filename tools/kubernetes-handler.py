#!/usr/bin/python

import kubernetes
import yaml
import os

if __name__ == '__main__':
    import argparse

DOCKER_ENGINE_PORT = 2376
__DEBUG__ = 0
DOCKER_CMD = 'docker.original'


class KubernetesHandler:
    """
    kubernetes client.
    Access the Kubernetes manager and retrieve the pod informations,
    then update the host_vars/* files ethernet configurations.
    """

    def __init__(self):
        self._set_arguments()
        self._load_kubernetes_configuration()
        self._coreV1Api = kubernetes.client.CoreV1Api()
        self._load_pods()

    def _set_arguments(self):
        if __name__ == '__main__':
            parser = argparse.ArgumentParser()
            parser.add_argument('--server', help="the apiserver url")
            parser.add_argument(
                '--host_vars',
                help="""
                specify the location of the host_vars files
                """
            )
            parser.add_argument(
                '--kube_config',
                help="specify the kubectl configuration file"
            )
            self._args = parser.parse_args()

    def _load_kubernetes_configuration(self):
        if self._args.server:
            kubernetes.client.Configuration().host = self._args.server
        elif self._args.kube_config:
            kubernetes.config.load_kube_config(self._args.kube_config)
        else:
            kubernetes.config.load_kube_config(
                os.path.join(os.environ['HOME'], '.kube/config'))

    def check_args(self):
        """command line mode: check argument and execute tasks"""
        if __name__ == '__main__':
            if self._args.host_vars:
                self.update_host_vars_addresses(self._args.host_vars)
            else:
                print "No action {}".format(self._args)

    def _load_pods(self):
        '''load data from kubernetes cluster'''
        self._by_hostnames = {}
        self._by_namespaces = {}
        _pods = self._coreV1Api.list_pod_for_all_namespaces()
        for pod in _pods.items:
            # print "load_pods: pod={}".format(pod.metadata.name)
            self._by_hostnames[pod.metadata.name] = pod
            if pod.metadata.namespace not in self._by_namespaces:
                self._by_namespaces[pod.metadata.namespace] = [pod]
            else:
                self._by_namespaces[pod.metadata.namespace] += [pod]

        print """
========= kubernetes environment ========
            pods={}
            namespaces={}
=========================================
        """.format(
            len(_pods.items),
            len(self._by_namespaces)
        )

    def update_host_vars_addresses(self, host_vars_path):
        for pod in self._by_hostnames:
            # print "update_host_vars: pod=", self._by_hostnames[pod]
            file_path = '{}/{}'.format(
                host_vars_path,
                self._by_hostnames[pod].metadata.name
            )
            address = self._by_hostnames[pod].status.pod_ip
            print 'file_path={},address={}'.format(file_path, address)
            try:
                with open(file_path, 'r') as infile:
                    data = yaml.load(infile)
                    if not data['ethernet_interfaces']:
                        print "No ethernet_intercafes node available, skip"
                        continue
                    for eth_if in data['ethernet_interfaces']:
                        eth_if['address'] = address
                    infile.close()
                    tmpfile = '{}.tmp~'.format(file_path)
                    with open(tmpfile, 'w') as outfile:
                        yaml.dump(
                            data,
                            outfile,
                            default_flow_style=False,
                            encoding='utf-8'
                        )
                        outfile.close()
                        os.rename(tmpfile, file_path)
            except IOError as e:
                print e


if __name__ == '__main__':
    try:
        sh = KubernetesHandler()
        sh.check_args()
    except KeyboardInterrupt:
        print "exit"
