#!/bin/bash

# this script execute the deployment of the Hirundo (Migration Service)
#
# author: yari.latini@synchronoss.com

# REINSTALL PROCEDURE

# different playbook could have different tags from Merlin you may want to skip
# set below the comma separated list of tags you want to ignore
TAGBL="--skip-tags=hostname,timezone,userpermissions,users,ntp,dns,limits,hosts"
# this is the playbook you will execute
PLAYBOOK="morgana-hirundo.yml"

# parse options and execute
source $(dirname $0)/lib.sh

# Useful commands
#
# START / STOP SERVICES ================================================
#
# MONITORING COMMANDS ==================================================
#

