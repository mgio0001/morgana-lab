#!/bin/bash

# this script execute the deployment of the SSL steps related to
# - PAB/CAL
# - POP/IMAP
# - UXFE
#
# author: yari.latini@synchronoss.com

# Before this script, please execute:
# 1) prod00-common.sh
# 2) prod01-confserv.sh
# 3) prod02-dirserv.sh
# 4) prod02-sdiscovery.sh
# 5) prod03-mos.sh
# 6) prod03-mss.sh
# 7) prod04-qservice.sh
# 8) prod05-affinity.sh
# 9) Plus PAB/CAL/POP/IMAP/WebUI (and DAC?)

# different playbook could have different tags from Merlin you may want to skip
# set below the comma separated list of tags you want to ignore
TAGBL="--skip-tags=owm-webtop-rpmforge,owm-webtop-webmail,hostname,timezone,userpermissions,users,ntp,dns,limits,hosts"
# this is the playbook you will execute
PLAYBOOK="morgana-ssl.yml"

# parse options and execute
source $(dirname $0)/lib.sh


# Useful commands
#
# START / STOP SERVICES ================================================
#
# CONFIGURATION CHANGES ================================================
#
# MONITORING COMMANDS ==================================================
#
# Check process are up
# ansible -i morgana-lab -b -f1 -m shell -a 'runuser -l imail -c imservping' <target-host>
# 
# Check processes (imconfserv and immgrsev MUST be up and running)
# ansible -i morgana-lab -b -f1 -m shell -a 'runuser -l imail -c imservping' <target-host>
#
# Check current log files for Errors/Warning/Critical messages
# ansible -i morgana-lab -b -f1 -m shell -a 'runuser -l imail -c "find log/ -name \*.log -type l | xargs grep -v Note | imlogprint -m"' mss
#
# CHECK MAINTENANCE ====================================================
#
# CONFIGURATION CHECKS =================================================
#
