#!/bin/bash

# this script execute the deployment of the SMSGW
#
# author: gary.palmer@synchronoss.com

# REINSTALL PROCEDURE
#
# in order to reset the changes from this playbook, execute the command below (stop primary as last if you're reinstalling everything):
#
# ansible -i <inventory-file> -b -f1 -m shell -a 'systemctl stop smsgw' <target_host>
#
# remove installation
# ansible -i morgana-lab -b -m shell -a 'rm -rf /opt/sms/Linux' <target-host>
# ansible -i morgana-lab -b -m shell -a 'rm -rf /opt/sms/SNMP_AGT' <target-host>
# ansible -i morgana-lab -b -m shell -a 'rm -rf /opt/sms/npm' <target-host>
#

# different playbook could have different tags from Merlin you may want to skip
# set below the comma separated list of tags you want to ignore
TAGBL=""
# this is the playbook you will execute
PLAYBOOK="morgana-smsgw.yml"

# parse options and execute
source $(dirname $0)/lib.sh

# XXX TODO XXX
# Useful commands
#
# START / STOP SERVICES ================================================
#
# Restart the instances (important -f1, stop primary as last and start primary as first)
# ansible -i <inventory-file> -b -f1 -m shell -a 'systemctl stop smsgw' <target_host>
#
# CONFIGURATION CHANGES ================================================
#
# Deployment of a new UX
# ./morgana-90-smsgw.sh --tags owm-smsgw --target <host-id>
#
# MONITORING COMMANDS ==================================================
#
# CHECK MAINTENANCE ====================================================
#
# CONFIGURATION CHECKS =================================================
#
