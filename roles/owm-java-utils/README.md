# owm-cassandra-utils

Role to install some useful tools in order to debug cassandra.

* troy-consistency-tool-1.0.0-09-SNAPSHOT-jar-with-dependencies.jar - Java Dump Tool to take mailbox dump from cassandra. Usage: java -jar /usr/local/lib/merlin-java-utils/troy-consistency-tool.jar <CassMDhost> <CassMDport> <CassBShost> <CassBSport> <mailboxId>

* jmxterm - JMX command-line tool. Usage: java -jar /usr/local/lib/merlin-java-utils/jmxterm.jar

* sjk-plus-0.4.2.jar - Swiss Java Knife - Great Java Diagnostic (see https://github.com/aragozin/jvm-tools). Usage: java -jar /usr/local/lib/merlin-java-utils/sjk.jar <cmd> <arguments>

