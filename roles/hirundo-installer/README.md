Role Name
=========

Migration Service Installer.

On the migration node there will be also a running mss and mxos that will register on eureka as MIGMSS and MIGMAILBOXSERVICE.
Migration service uses these local mss and mxos during migration to avoid traffic against the production nodes.

Requirements
------------

Role Variables
--------------

```yaml
hirundo_database_service: 'postgresql'
hirundo_package: 'Hirundo-1.0.6-bin.tgz'
hirundo_release: 'Hirundo-1.0.6'
hirundo_deployment_dir: 'hirundo'

hirundo_user: hirundo
hirundo_password: hirundo
hirundo_database: hirundo

hirundo_manager_username: manager
hirundo_manager_password: password

postgres_user: postgres
postgres_hba_conf: /var/lib/pgsql/data/pg_hba.conf

hirundo_agent_data_path: /custom/path/to/migration/agent/data (default /opt/imail/hirundo-agent/data)
hirundo_master_data_path: /custom/path/to/migration/master/data (default /opt/imail/hirundo-master/data)
```

Host having master='true' will have master role (it will also install postgresql), host having agent='true' will have only agent deployed.
NB: the master node has also a local agent node, so there is no need to specify agent=true on a node having master=true

Tests
--------------

Rest Login:
```bash
curl -c /tmp/`echo .$$` -b /tmp/`echo .$$`  'http://localhost:1972/management/login' -H 'X-Requested-With: XMLHttpRequest' --data 'username=manager&password=password' --compressed
```

Add Node:
```bash
curl -c /tmp/`echo .$$` -b /tmp/`echo .$$` 'http://localhost:1972/management/executors/add' -X PUT -H 'X-Requested-With: XMLHttpRequest' --data 'label=master&host=migm01a&port=10000&enabled=on&maxParallel=5' --compressed
```

List nodes:
```bash
curl -c /tmp/`echo .$$` -b /tmp/`echo .$$` 'http://localhost:1972/management/executors/state' -X GET -H 'X-Requested-With: XMLHttpRequest' --compressed | python -m json.tool
```

Node metrics (check metrics for agent 1):
```bash
curl -c /tmp/`echo .$$` -b /tmp/`echo .$$` 'http://localhost:1972/management/executor/1/metrics' -H 'X-Requested-With: XMLHttpRequest' --compressed | python -m json.tool
```

Remove node (delete agent 1):
```bash
curl -c /tmp/`echo .$$` -b /tmp/`echo .$$` 'http://localhost:1972/management/executor/1/delete' -X POST -H 'X-Requested-With: XMLHttpRequest' --compressed
```

Add Task script (TaskNameHere):
```bash
curl -c /tmp/`echo .$$` -b /tmp/`echo .$$` 'http://localhost:1972/management/tasks/TaskNameHere/add' -X PUT -H 'X-Requested-With: XMLHttpRequest' --data-binary $'var n = Math.random();\njava.lang.Thread.sleep(10000 * n);\nif (n>0.5) {\nthrow "Job failed (JS throw <exception>)";\n}' --compressed
```

List tasks:
```bash
curl -c /tmp/`echo .$$` -b /tmp/`echo .$$` 'http://localhost:1972/management/tasks' -H 'X-Requested-With: XMLHttpRequest' --compressed | python -m json.tool
```

Delete Task:
```bash
curl -c /tmp/`echo .$$` -b /tmp/`echo .$$` 'http://localhost:1972/management/tasks/TaskNameHere/delete' -X DELETE -H 'X-Requested-With: XMLHttpRequest' --compressed
```

Add Job script:
```bash
curl -c /tmp/`echo .$$` -b /tmp/`echo .$$` 'http://localhost:1972/management/jobdefs//jobtest/add' -X PUT -H 'X-Requested-With: XMLHttpRequest' --data-binary $'var n = Math.random();\njava.lang.Thread.sleep(10000 * n);\nif (n>0.5) {\nthrow "Job failed (JS throw <exception>)";\n}' --compressed
```
