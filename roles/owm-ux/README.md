# owm-ux

Role to install or update uxsuite package and configure it.
It stops the mx services, remove the existing package and replace it with the new one.
Log files are preserved because WEB-INF/logs directory is replaced by a symbolik link pointing to the $INTERMAIL/log/webtop/{{ux_context_path}} directory (while the ux is installed under $INTERMAIL/webtop/webapps/{{ux_context_path}}.

If doesn't manage the update on a different ux_context_path, so existing path will remain and a new webapplication will be deployed aside the existing one.

# vars configuration

```yaml
# name of the ux project package to install
# it will be downloaded from {{owm_tar_url}}/{{ux_package_name}}
# So make sure you package is available from the configured tarball repository
ux_package_name: 'kiwi-2.1.204-17.war'

# UX ServletContextPath
ux_context_path: 'ux'

# mxos configuration. Defaults is usually good. It will point to the locally
# installed HAProxy node. You may want to update the connections parameters.
mxos_url: 'http://127.0.0.1:18081/mxos'
mxos_maxconns: '50'
mxos_connto: '120s'
mxos_readto: '60s'

# Webtop Statistics configuration
# These defaults are suitable for the health station
webtop_stats_update_interval: 5s
webtop_stats_report_interval: 60s
webtop_stats_enabled: 'true'

# MediaPreview configuration. Defaults are usually good. Change them
# if you don't want to point to the locally installed mediapreview service.
office_preview_uri: 'http://127.0.0.1:8080/webtop-media'
office_preview_maxconns: '200'
office_preview_connto: '5s'
office_preview_sockto: '30s'

# Hugemail Configuration.
# It usually points to the locally installed HAProxy
# URLs should be updated to match your production deployment
# (replace the 127.0.0.1/kiwi with the public VIP and URL that is 
# responding to the Internet traffic)
hugemail_enabled: 'false'
hugemail_resource_url: 'http://127.0.0.1:18082/jsonrpc2'
hugemail_message_password: 're6quo!FaS}och7jAip7ahm:gui|Y0ikla_rii6Ayah-Xu9a'
hugemail_upload_url: 'http://127.0.0.1:18082/upload'
hugemail_download_url: 'http://127.0.0.1:18082/download'
hugemail_pickup_url: 'http://public:8080/kiwi/bin'
hugemail_default_route: '127.0.0.1:18082'

# IMAP configuration
store_imap_host: '127.0.0.1'
store_imap_port: '10143'
store_proxy_auth_enabled: 'false'
store_proxy_auth_username: ''
store_proxy_auth_password: ''
transport_smtp_host: '127.0.0.1'
transport_smtp_port: '10025'
transport_smtp_auth: 'false'

# Service Discovery on UX is currently available but
# not supported
service_discovery_enabled: 'false'

# Redirects, update them to point to the VIP and URLs
# exposed to the Internet
logout_url: 'http://localhost:8080/ux/'
timeout_url: 'http://localhost:8080/ux/'
rsvp_url: 'http://public/ux/rsvp'
share_cal_url: 'http://public/ux/http/shareCalendar'

# Webtop Cookie configuration
# (defaults are fine, change this if you know what you're doing)
# You may want to set cookie_security_flag to 'true' to improve security if
# you're serving your UX from HTTPS only
cookie_security_flag: 'false'
cookie_name_prefix: 'webtop'
cookie_domain: ''
cookie_security_flag: 'false'
cookie_version: '1'

# Calendar configuration
caldav:
  host: '127.0.0.1'
  port: 5229
  uri: '/calendars'
  trash: 'true'
  proxyHost: '127.0.0.1'
  proxyPort: 5229
  asAdmin: 'true'
  
```