Role Name
=========

This role implements the shared install/upgrade logic and inherits the configuration logic from the below roles:

* owm-affinity-manager-configdb
* owm-cal-configdb
* owm-cal
* owm-common
* owm-confserv
* owm-dircacheserv
* owm-dirserv
* owm-eureka
* owm-extserv
* owm-imap
* owm-mos-configdb
* owm-mos
* owm-mss-configdb
* owm-mss
* owm-mta-configdb
* owm-mta
* owm-nginx
* owm-pab-configdb
* owm-pab
* owm-platformtools
* owm-pop
* owm-queue-admin-configdb
* owm-queue-admin
* owm-queue-search-configdb
* owm-queue-search
* owm-queue-service-configdb
* owm-queue-service
* owm-queue
* sncr-search

It proceeds by installing all the required packages on the host (even thought it belongs to multiple groups).

The list of the installed packages depends by the groups the host belongs to and by the defined configuration key "rpms_data".

The tasks that are required before and after installing the rpm package are managed by 3 different inclusiong levels:

* host level: tasks/rpms/post/main.yml and tasks/rpms/pre/main.yml
* group level: tasks/rpms/post/groups/<group>.yml and tasks/rpms/pre/groups/<group>.yml
* tags level: tasks/rpms/post/tags/<tag>.yml and tasks/rpms/pre/tags/<tags>.yml
* package level: tasks/rpms/post/packages/<package.id>.yml and tasks/rpms/pre/packages/<package.id>.yml

The original task logic from the old roles is included from tasks/rpms/confs/<package.id>.yml

The original config.db tasks are all part of the owm-confserv deployment and are executed when this package is installed (see templates/owm-confserv/ for the original j2 templates and tasks/rpms/confs/owm-confserv.yml for the execution logic.



Requirements
------------

CentOS/RH6 or CentOS/RH7 are supported. Install and configure the datastore nodes (owm-cassmeta and owm-cassblob). If you're planning to install the queue search it will be also required to have elasticsearch configured (owm-qelk-search).

This role needs from 1GB to 4GB of memory to run (the more packages are configured on the same host, the higher the required memory)

Role Variables
--------------

The role applies a shared set of environment variables while executing the rpm tool, those variables are detailed in vars/main.yml:

```yaml
rpms_envs: {
  # this is shared among all the package
  shared_cmd_install_env:     'AUTOSTART=0 INSTALL_USER=# INSTALL_GROUP=# GROUP_SITE=# EUREKA_HOSTS=# INTERMAIL=# DOMAINNAME=# ',
  # this is shared among all the packages by excluding the primary confserv
  confclient_cmd_install_env: 'CONFSERVHOST=# CONFSERV_ADMINPORT=# CONFSERV_PORT=# ',
  conserver_cmd_install_env:  'STANDBYCONFSERVHERE=# CONFCACHESERVHERE=# '
}
```
The role replaces the '#' keys with the right information from the groups, hosts and inventory.

The role reads from vars/main.yml rpms install/upgrade instructions, where they are detailed as <package_id>_package dict object, e.g.:

```yaml
owm_common_package: {
        name: 'owm-common-#emailmx_version#.el6.x86_64',
        id: 'owm-common',
        rpm_id: 'owm-common',
        tag: 'emailmx',
        cmds: {
          install: 'rpm -ivh --prefix=#emailmx_home# #rpm_url#/owm-common-#emailmx_version#.el6.x86_64.rpm',
          upgrade: 'UPGRADE=1 rpm -Uvh --prefix=#emailmx_home# #rpm_url#/owm-common-#emailmx_version#.el6.x86_64.rpm'
        }
      }
```
where:

* name: this is the name of the package as it is identified by rpm/yum (role checks this in order to verify the exact version installed when package is already present)
* id: this is the generic name of the package, the role looks for <id>.yml files in rpms/confs/ rpms/pre/packages/ rpms/post/packages
* rpm_id: this is the generic name of the rpm package (role checks this in order to verify if the package is already installed with rpm -q)
* tag: this is to group under a shared pre/post tasks multiple packages that need the same pre/post actions, e.g. emailmx (for all the core emailmx services). The role will then includes and execute tasks/rpms/pre/tags/<tag-name>.yml before running the rpm/yum tools and then will include and execute the tasks/rpms/post/tags/<tag-name>.yml after running the rpm/yum tools.
* cmds: this includes install and upgrade commands for the package. When no cmds is provided the role will use "yum install/upgrade <package.id>" by using the configure repos.

It is then possible to refer to the above object in order to build a deployment structure. By default role offers a ready to use Mx 9.2 deployment structure having stateless queue service and stateless pab and cal configured. e.g.:

```yaml

rpms_data: {
  mx: {
    packages: [
      "{{owm_common_package}}"
    ]
  },
  directory: {
    packages: [
      "{{owm_confserv_package}}",
      "{{owm_service_discovery_package}}",
      "{{owm_sur_common_package}}",
      "{{owm_sur_master_package}}",
# Mx 9.2 doesn't use sur_cache anymore
#      "{{owm_sur_cache_package}}",
      "{{owm_mxos_thirdparty_package}}",
      "{{owm_mxos_package}}",
      "{{owm_platformtools_package}}"
    ]
  },
  secondarydirectory: {
    packages: [
      "{{owm_sur_common_package}}",
      "{{owm_sur_master_package}}",
# Mx 9.2 doesn't use sur_cache anymore
#      "{{owm_sur_cache_package}}",
      "{{owm_mxos_thirdparty_package}}",
      "{{owm_mxos_package}}",
      "{{owm_platformtools_package}}"
    ]
  },
  affinity: {
    packages: [
      "{{owm_nginx_package}}"
    ]
  },
  mss: {
    packages: [
      "{{owm_msg_common_package}}",
      "{{owm_msg_mss_package}}"
    ]
  },
  qadminsearch: {
    packages: [
      "{{owm_queue_admin_package}}",
      "{{owm_queue_search_package}}"
    ]
  },
  qservice: {
    packages: [
      "{{owm_queue_service_package}}"
    ]
  },
  fep: {
    packages: [
      "{{owm_msg_common_package}}",
      "{{owm_msg_mta_package}}",
#      "{{owm_msg_extserv_package}}",
      "{{owm_msg_imapserv_package}}",
      "{{owm_msg_popserv_package}}"
    ]
  },
  pabcal: {
    packages: [
      "{{owm_pab_package}}",
      "{{owm_cal_package}}"
    ]
  }
}

```

The first level keys of the rpms_data are the groups grouping the packages that will be installed. On execution the role will read from the object rpms_seq which groups and with which sequence the packages will be installed, from the defaults/main.yml

```yaml
rpms_seq:
  - mx
  - directory
  - secondarydirectory
  - affinity
  - mos
  - mss
  - qadminsearch
  - qservice
  - fep
  - pabcal
```

By following the order on the above object, the role will proceed by looking at the first level keys in the rpms_data object that will follow the below format:

```yaml

<inventory_group>: { <-- this is the group id, it also matches the inventory groups
   packages: [ <-- this is an array of object containing the package details (see below)
   {
     name: 'name_of_the_rpm_package_without_extension',
     id: 'generic_package_name',
     rpm_id: 'generic_rpm_package_name',
     tag: 'a generic tag that group multiple packages having common pre/post tasks',
     cmds: { <-- this dict includes the install/update commandline, <options>=# or #rpm_url# or #emailmx_version# are replaced with regex_replace filter
       install: 'the_rpm_command_to_use_for_install',
       upgrade: 'the_rpm_command_to_use_for_upgrade
     }
 ...
```

Dependencies
------------

This role depends by the following roles:

* owm-localrepository (to configure and enable management nodes repositories)
* common (to properly setup controlled host)
* owm-homepermissions (to fix mounts before proceeding)
* jdk (it installs the required jdk)
* haproxy (it installs and configure haproxy where needed)


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yaml
- hosts: directory
  roles:
    - { role: mx-installer, when: primary is defined and primary == 'true' }
  tags:
    - emailmx
    - emailmx-primary

- hosts: mx
  serial: 1
  roles:
    - { role: owm-postfix, when: "inventory_hostname in groups['fep']" }
    - { role: mx-installer, when: "not (inventory_hostname in groups['directory'] and primary is defined and primary == 'true')" }

```    

Contribution
------------

In order to add new package to this role, it is required to proceed like below:

* create a <package_id>_package entry in the var/main.yml by including
** id
** name
** tag
** cmds node (optionally) if the new package requires rpm call and yum install/upgrade can't handle the deployment
* update rpms/version.yml, rpms/storage.yml and rpms/locations.yml if new placeholders are required
* if a new tag is defined add a <tag_id>.yml file under rpms/pre/tags and rpms/post/tags. This file is required, if no specific tasks are needed on pre/post installation, just add an empty file with '---'
* if a new group is defined add a <group_id>.yml file under rpms/pre/groups and rpms/post/groups. This file is required, if no specific tasks are needed on pre/post installation, just add an empty file with '---'
* if a new group is defined, make sure that new group is also included in the rpms_data and rpms_seq configuration object
* create a <package_id>.yml under rpms/conf/ with any customer tasks this role has to execute in order to configure the package, if no tasks is expected, just create an empty file with '---'. If additional includes are required, please make sure to add them (and include them) from a subfolder having <package_id> as name to keep the project clean and readable.
* create a <package_id>.yml under rpms/pre/packages and rpms/post/packages with any specific pre/post tasks required before installing / upgrading the package. If no task is required, just create an empty file with '---'
* any script to execute in order to set new keys into the config.db can be stored under templates/owm-confserv, while the related tasks have to be added to the tasks/rpms/confs/owm-confserv.yml file

While writing the pre/post tasks, please bear in mind there is a number of variables that can be used in order to understand the installation/upgrade flow:

* package_installed - boolean. True=package.id is present, False=package.id is not present
* package_upgraded - boolean. True=package.id is installed and upgraded to the latest version, False=package.id is installed but not up dated
* is_centos7 - boolean. System is centos7, systemd based service start / stop is recommended.
* is_not_centos7 - boolean. System is not a centos7, no systemd support available.
* node_site - string. This is the site the inventory_hostname belongs to
* node_cluster - string. This is the cluster_id the inventory_hostname belongs to
* conf_server_name - hostname of the master confserv
* conf_server_port - number.
* conf_server_admin_port - number.
* eureka_hosts - string. List of eureka host in the form of <host>:<port>,<host>:<port>
* eureka_url - string, List of eureka hosts in the form of http://<host>:<port>/eureka/,http://<host>:<port>/eureka/
* domain_name - string, the default name
* mss_hostname - string, the affinity manager MSS hostname
* pabcal_hostname - string, the list of the cassandra nodes that are part of the pab/cal ring
* mOS_hostname - string, the affinity manager MxOS hostname
* dircache_hosts - string, the list of the available dircache (if any)
* queue_server_hosts - string, the list of the qserv nodes (if any)

See tasks/init/init-check.yml to check which parameters and variables

Author Information
------------------

Yari Latini Corazzini - Solutions Architect @ Synchronoss - yari.latini@synchronoss.com
