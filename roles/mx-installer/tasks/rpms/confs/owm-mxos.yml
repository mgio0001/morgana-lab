---
- name: Put the hazelcast.xml file.
  template: src=owm-mos/hazelcast-mxos.xml.j2 dest={{emailmx_user[0].home}}/mxos/config/hazelcast/hazelcast-mxos.xml
  when: "inventory_hostname in groups[node_site+'-queue'] or inventory_hostname in groups[node_site+'-mos']"

- name: Update Schema Attributes for SMS Compose (msisdn)
  template:
    src: 'owm-mos/msisdn.j2'
    dest: '{{emailmx_user[0].home}}/mxos/config/mxos/schema/attributes/msisdn'
    group: "{{ emailmx_user[0].username if users_create_per_user_group else users_group }}"
    owner: '{{emailmx_user[0].username}}'
    mode: 0755
  when: 'include_msisdn is defined'

- name: Update Mailbox Rules for SMS Compose (msisdn)
  replace:
    dest: '{{emailmx_user[0].home}}/mxos/config/mxos/mailbox-mxos-rules.xml'
    regexp: '(.*)<param name="smsServicesMsisdn" type="phoneNumber" errorCode="MBX_INVALID_SMS_SERVICES_MSISDN"/>(.*)'
    replace: '\1<param name="smsServicesMsisdn" type="msisdn" errorCode="MBX_INVALID_SMS_SERVICES_MSISDN"/>\2'
  when: 'include_msisdn is defined'


- name: Change the log level from INFO to WARN
  replace: dest={{emailmx_user[0].home}}/mxos/config/log4j.xml regexp='<priority value="INFO" />' replace='<priority value="WARN" />'

- name: Change log location
  replace:
    dest: "{{emailmx_user[0].home}}/mxos/config/log4j.xml"
    regexp: '(.*?)"File"(.*?)value="(.*?)/mxos\.(.*?)"'
    replace: '\1"File"\2value="{{ mx_logs_path }}/mxos.\4"'
  when: mx_logs_path is defined

- name: Raise MaxFileSize log
  replace:
    dest: "{{emailmx_user[0].home}}/mxos/config/log4j.xml"
    regexp: 'name="MaxFileSize"(.*?)value=".*?"'
    replace: 'name="MaxFileSize"\1value="{{ mos_logs_file_size }}"'
    backup: yes

- name: Check if {{emailmx_user[0].home}}/mxos/config/mxos.properties is installed
  stat: path={{emailmx_user[0].home}}/mxos/config/mxos.properties
  register: mos_properties_file_stat

- name: Update the '/etc/sysconfig/prelink' file
  lineinfile: dest=/etc/sysconfig/prelink regexp='^PRELINKING=' line='PRELINKING=no' state=present create=yes

- name: Change storeUserNameAsEmail property value
  replace: dest={{emailmx_user[0].home}}/mxos/config/mxos.properties regexp='storeUserNameAsEmail=false' replace='storeUserNameAsEmail=true'
  when:
    - mos_properties_file_stat.stat.exists

- name: Change returnUserNameWithDomain property value
  replace: dest={{emailmx_user[0].home}}/mxos/config/mxos.properties regexp='returnUserNameWithDomain=false' replace='returnUserNameWithDomain=true'
  when:
    - mos_properties_file_stat.stat.exists

- name: Change calendarStoreIntegrated property value
  replace: dest={{emailmx_user[0].home}}/mxos/config/mxos.properties regexp='calendarStoreIntegrated=true' replace='calendarStoreIntegrated=false'
  when:
    - mos_properties_file_stat.stat.exists

- name: Change addressBookStoreIntegrated property value
  replace: dest={{emailmx_user[0].home}}/mxos/config/mxos.properties regexp='addressBookStoreIntegrated=true' replace='addressBookStoreIntegrated=false'
  when:
    - mos_properties_file_stat.stat.exists

- name: Increase the number of maxThreads
  replace:
    dest: '{{emailmx_user[0].home}}/mxos/server/conf/server.xml'
    regexp: '{{item.0}}'
    replace: '{{item.1}}'
  with_together:
    - [ '(\s+)maxThreads="(\d+)"(\s+)' ]
    - [ '\1maxThreads="{{mos_max_threads}}"\3']

- name: Set Max HEAP
  lineinfile:
    dest: '{{emailmx_user[0].home}}/mxos/server/bin/setenv.sh'
    insertbefore: "(.*)Auto-detect memory sizing"
    line: "MAX_HEAP={{mos_max_heap}}"
  when: 'mos_max_heap is defined'

- name: Set Min HEAP
  lineinfile:
    dest: '{{emailmx_user[0].home}}/mxos/server/bin/setenv.sh'
    insertbefore: "(.*)Auto-detect memory sizing"
    line: "MIN_HEAP={{mos_min_heap}}"
  when: 'mos_min_heap is defined'

- name: Set PERMSIZE
  lineinfile:
    dest: '{{emailmx_user[0].home}}/mxos/server/bin/setenv.sh'
    insertbefore: "(.*)Auto-detect memory sizing"
    line: "PERM_SIZE={{mos_perm_size}}"
  when: 'mos_perm_size is defined'

- name: Enabling Kernel 4 support
  lineinfile:
    dest: '{{emailmx_user[0].home}}/mxos/server/bin/setenv.sh'
    backrefs: 'yes'
    regexp: 'if \[ \$\{version:0:1\} == "3" \]'
    line: 'if [ ${version:0:1} == "4" ]'
    backup: 'yes'
  when: 'ansible_kernel|version_compare("4",">=")'

# TODO: import_task
- include: owm-mxos/centos7.yml
  when: is_centos7

# TODO: import_task
- include: owm-mxos/centos6.yml
  when: is_not_centos7
