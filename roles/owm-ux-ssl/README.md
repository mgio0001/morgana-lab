# owm-ux-ssl

Installs an NGINX on the uxsuite machine and a certificate (Server Certificate, Server Key and CA chain) to terminate SSL/TLS traffic. It acts as a reverse proxy and forwards the traffic to the Tomcat on same machine.
No modification or configuration change on the Tomcat required.  


## Required customer specific data
It requires an archive file with the following files in it:
- Server Key
- Server Certificate
- CA Certificate Chain

The archive is retrieved from {{owm_tar_url}}.


## Group vars configuration
The name of the archive and the files in it are configured in:
group_vars/uxsuite.

Additionally the SSL deployment and configuration can be enabled/disabled:
ssl_uxsuite:   True | False


## Verify Installation
Verify with openssl, make seu DN is correct and no error is shown. Replace with your hostname:

__HTTPS:__
openssl s_client -port 8443 -host a1ux01
