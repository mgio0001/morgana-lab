Role Name
=========

This roles perform some post-installation tasks on the installed uxsuite nodes.

Requirements
------------

This role expects to have all the uxsuite nodes already installed and configured through the owm-webtop and owm-ux roles.

Role Variables
--------------

This role requires to have emailmx_user object configured and pointing to the owner of the webtop deployment (e.g. /opt/imail): 

```yaml
emailmx_user:
  - username: imail
    password: $6$dIY1kl2.$ZT2PkxSD4snwdReiKgg.OKVWRVlz29NWK347bHWqDQaamtggtzqqkORNv0aQyKsg1eUsiq.xEQPlIsa.qrB4P/
    name: Email Mx imail account
    home: /opt/imail
    groups: []
    uid: 500
    ssh_key: []
```

Dependencies
------------


- owm-webtop

