# owm-localrepository

This is a custom role created in order to work-around the lack of communication with
the Internet from the customer environment.

It provides 3 yum repositories:
* repo - the rpms repository with OWM/SNCR software
* centos - the rpms repository with the RHEL/CentOS packages required (including EPEL/REMI)
* ansible - the rpms repository with the package needed for the ansible tools deployment

Additionally this role also fixes a misconfiguration issue in the RHEL repository (satellite?)
available in the platform of the customer.

# configuration keys:
owm_repo_root: - the leading part of the http URL managed nodes will use in order to access their local managemente nodes.
```
owm_repo_root: http://10.0.2.15/repo/
```
That URL should point to the management node or to the hosts where you have installed the local repository, see [Management node setup](https://bitbucket.org/openwave/morgana/wiki/Management%20node%20setup.)

# test
Once installed every managed host will have 3 new /etc/yum.repos.d/owm-*.repo files, check this with
```bash
ansible -i <inventory> -m shell -a 'ls -la /etc/yum.repos.d/owm-*.repo' all -b
```
Output will return something like
```
mx921data | SUCCESS | rc=0 >>
-rw-r--r--. 1 root root 130 Mar 28 18:12 /etc/yum.repos.d/owm-ansible.repo
-rw-r--r--. 1 root root 127 Mar 28 18:12 /etc/yum.repos.d/owm-centos.repo
-rw-r--r--. 1 root root 109 Mar 28 18:12 /etc/yum.repos.d/owm-repo.repo

mx921qdata | SUCCESS | rc=0 >>
-rw-r--r--. 1 root root 130 Mar 28 18:12 /etc/yum.repos.d/owm-ansible.repo
-rw-r--r--. 1 root root 127 Mar 28 18:12 /etc/yum.repos.d/owm-centos.repo
-rw-r--r--. 1 root root 109 Mar 28 18:12 /etc/yum.repos.d/owm-repo.repo

mx921access | SUCCESS | rc=0 >>
-rw-r--r--. 1 root root 130 Mar 28 18:13 /etc/yum.repos.d/owm-ansible.repo
-rw-r--r--. 1 root root 127 Mar 28 18:13 /etc/yum.repos.d/owm-centos.repo
-rw-r--r--. 1 root root 109 Mar 28 18:13 /etc/yum.repos.d/owm-repo.repo

mx921service | SUCCESS | rc=0 >>
-rw-r--r--. 1 root root 130 Mar 28 18:13 /etc/yum.repos.d/owm-ansible.repo
-rw-r--r--. 1 root root 127 Mar 28 18:12 /etc/yum.repos.d/owm-centos.repo
-rw-r--r--. 1 root root 109 Mar 28 18:12 /etc/yum.repos.d/owm-repo.repo
```

Also, yum repolist command, executed on the controlled hosts should list the new owm-* repos
```
ansible -i morgana-lab -m shell -a 'yum repolist' all
mx921service | SUCCESS | rc=0 >>
Loaded plugins: fastestmirror
Determining fastest mirrors
repo id                 repo name                                         status
!owm-ansible            OWM Ansible Control Node repository                43
!owm-centos             OWM CentOS Essentials repository                  648
!owm-repo               OWM Mx repository                                  51
!spacewalk-client       Spacewalk Stuff needed by all vm's - x86_64       158
repolist: 900

mx921access | SUCCESS | rc=0 >>
Loaded plugins: fastestmirror
Determining fastest mirrors
repo id                repo name                                          status
owm-ansible            OWM Ansible Control Node repository                 43
owm-centos             OWM CentOS Essentials repository                   648
owm-repo               OWM Mx repository                                   51
spacewalk-client       Spacewalk Stuff needed by all vm's - x86_64        158
repolist: 900

mx921qdata | SUCCESS | rc=0 >>
Loaded plugins: fastestmirror
Determining fastest mirrors
repo id                repo name                                          status
owm-ansible            OWM Ansible Control Node repository                 43
owm-centos             OWM CentOS Essentials repository                   648
owm-repo               OWM Mx repository                                   51
spacewalk-client       Spacewalk Stuff needed by all vm's - x86_64        158
repolist: 900
```