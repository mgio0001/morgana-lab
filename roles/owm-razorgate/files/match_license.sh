#!/bin/sh

LICENSE_PATH=$1
shift

cd $LICENSE_PATH
for MAC in "$@"; do
        if [ -f *$MAC* ]; then
                if [ -r *$MAC* ]; then
                        echo *$MAC*
                        exit 0
                fi
        fi
done
exit 1
