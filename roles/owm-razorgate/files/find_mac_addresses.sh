#!/bin/sh

. $RAZORGATE_HOME/.profile

AWK=/usr/bin/awk
ECHO=/usr/bin/echo
GREP=/usr/bin/grep
NMCLI=/usr/bin/nmcli
SED=/usr/bin/sed
XARGS=/usr/bin/xargs
IP_CMD=/usr/sbin/ip

/bin/systemctl is-active NetworkManager | grep -vq '^active'
NM_ACTIVE=$?

if [ $NM_ACTIVE -eq 1 ]; then
    INTERFACES=$($NMCLI device status | $AWK '/ethernet/ {print $1}' | $XARGS $ECHO)
    MACS=""

    if [ $# -eq 1 ]; then
        SSH_IP=$1
        for INT in $INTERFACES; do
            INT_SHOW=$($NMCLI device show $INT)
            MAC=$($ECHO "$INT_SHOW" | $AWK '/GENERAL\.HWADDR/ {print $2}' | $SED 's/:/-/g')
            IP_LIST=$($ECHO "$INT_SHOW" | $AWK '/IP4.ADDRESS\[/ {print $2}' | $SED 's/\/[0-9]*$//' | $XARGS $ECHO)
            for IP in $IP_LIST; do
                if [ "$IP" = "$SSH_IP" ]; then
                    if [ "x$MACS" = "x" ]; then
                        MACS=$MAC
                    else
                        MACS="$MACS $MAC"
                    fi
                fi
            done
       done
       echo "$MACS"
       exit 0
    elif [ $# -eq 2 ]; then
        SSH_IP=$1
        FILE=$2
        DIR=$(dirname "${FILE} ")
        for INT in $INTERFACES; do
            INT_SHOW=$($NMCLI device show $INT)
            MAC=$($ECHO "$INT_SHOW" | $AWK '/GENERAL\.HWADDR/ {print $2}' | $SED 's/:/-/g')
            IP_LIST=$($ECHO "$INT_SHOW" | $AWK '/IP4.ADDRESS\[/ {print $2}' | $SED 's/\/[0-9]*$//' | $XARGS $ECHO)
            MACFILE=$(ls $DIR/*${MAC}*)
            for IP in $IP_LIST; do
                if [ "$IP" = "$SSH_IP" ]; then
                    if [ "$MACFILE" = "$FILE" ]; then
                        echo Y | $MIRA_ROOT/usr/bin/install_license.sh $FILE $IP
                    fi
                fi
            done
        done
        exit 0
    fi
else
    INTERFACES=$($IP_CMD -o link show up | grep -v loop | awk -F : '{print $2}' | sed 's/ //g')
    MACS=""
    if [ $# -eq 1 ]; then
        SSH_IP=$1
        for INT in $INTERFACES; do
            MAC=$($IP_CMD addr show $INT | awk '/ether/ {print $2}' | tr '[:lower:]' '[:upper:]' | sed 's/:/-/g')
            if echo $MAC | grep -q '^[0-9A-F][0-9A-F]-[0-9A-F][0-9A-F]-'; then
                IP_LIST=$(ip -o addr show $INT scope global | grep inet | sed 's/^.*inet //;s/^.*inet6 //;s/ .*$//;s/\/[0-9]*//')
                for IP in $IP_LIST; do
                    if [ "$IP" = "$SSH_IP" ]; then
                        if [ "x$MACS" = "x" ]; then
                            MACS=$MAC
                        else
                            MACS="$MACS $MAC"
                        fi
                    fi
                done
            fi
        done
       echo "$MACS"
       exit 0
    elif [ $# -eq 2 ]; then
        SSH_IP=$1
        FILE=$2
        DIR=$(dirname "${FILE} ")
        for INT in $INTERFACES; do
            MAC=$($IP_CMD addr show $INT | awk '/ether/ {print $2}' | tr '[:lower:]' '[:upper:]' | sed 's/:/-/g')
            if echo $MAC | grep -q '^[0-9A-F][0-9A-F]-[0-9A-F][0-9A-F]-'; then
                IP_LIST=$(ip -o addr show $INT scope global | grep inet | sed 's/^.*inet //;s/^.*inet6 //;s/ .*$//;s/\/[0-9]*//')
                MACFILE=$(ls $DIR/*${MAC}*)
                for IP in $IP_LIST; do
                    if [ "$IP" = "$SSH_IP" ]; then
                        if [ "$MACFILE" = "$FILE" ]; then
                            echo Y | $MIRA_ROOT/usr/bin/install_license.sh $FILE $IP
                        fi
                    fi
                done
            fi
        done
        exit 0
    fi
fi
exit 1
