#!/bin/sh

# The docker environment seems not to have either NetworkManager or the
# "ip" command so make (probably wrong) assumptions that there is only
# one interface named eth0 and that the hostname is only listed once
# in /etc/hosts and it has the correct IP against it.

. $RAZORGATE_HOME/.profile

AWK=/usr/bin/awk
ECHO=/usr/bin/echo
GREP=/usr/bin/grep
NMCLI=/usr/bin/nmcli
SED=/usr/bin/sed
XARGS=/usr/bin/xargs
IP_CMD=/usr/sbin/ip
HEAD=/usr/bin/head
TR=/usr/bin/tr

if [ $# -eq 1 ]; then
    SSH_IP=$1
    MAC=$(cat /sys/devices/virtual/net/eth0/address | $SED 's/:/-/g' | tr '[:lower:]' '[:upper:]')
    echo "$MAC"
    exit 0
elif [ $# -eq 2 ]; then
    SSH_IP=$1
    FILE=$2
    DIR=$(dirname "${FILE} ")
    MAC=$(cat /sys/devices/virtual/net/eth0/address | $SED 's/:/-/g' | $TR '[:lower:]' '[:upper:]')
    HOSTNAME=$(hostname -s)
    IP=$($AWK "/$HOSTNAME/ {print \$1}" /etc/hosts | $HEAD -1)
    MACFILE=$(ls $DIR/*${MAC}*)
    if [ "$MACFILE" = "$FILE" ]; then
        echo Y | $MIRA_ROOT/usr/bin/install_license.sh $FILE $IP
    fi
    exit 0
fi
exit 1
