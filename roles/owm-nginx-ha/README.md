# owm-nginx-ha

Role to install Affinity manager and it's components that include pacemaker and corosync.

## Host vars configuration

Following variables needs to configured properly in host_vars/site1afnmss01 file. where site1afnmss01 is host file for affinity manager for mss for cluster 01 in site1

* clusterId: cluster01 - Cluster id
* VIP: 172.20.0.200 - VIP for mss (floating IP separate from the usual service VLAN IP)

For site1afnswift01, the host file for affinity manager for swift, the following variables need to be set:

* clusterId: cluster01 - mss cluster id
* swiftVIP: 172.20.0.201 - VIP for swift (floating IP separate from the usual service VLAN IP)

The failover node also needs the mss clusterID configured in its host_vars file.

All 3 nodes need to be set as

* component: 'failover'
* failover:
*   - 'mss'
*   - 'swift'

rather than the usual nginx component selection

In the group_vars for site1 or the host_vars for the individual affinity manager hosts in a site that requires more than one capacity cluster set

* affinity_cluster_name: AM_SITE1

which should be unique to that site and capacity cluster.

The role assumes that all 3 nodes in the failover cluster use the same network interface name to access the service VLAN.  It configures the MSS floating IP VIP on subinterface :1 and the swift floating IP VIP on subinterface :2 on the service VLAN.

## Role configuration

Following variables needs to be configured

* owm_nginx_version - nginx version defined in  group_vars/affinity
* mos_listening_port - mos Server listening port defined in  group_vars/affinity
* mss_listening_port - mss listening port defined in  group_vars/affinity
* owm_repo_url - defined in the 'group_vars/all' file.
