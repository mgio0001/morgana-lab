# owm-fep-ssl-configdb

Role to configure configdb for the owm-fep-ssl deployment. It has to be executed by targeting the master configuration servers. Also see


## Required customer specific data
See owm-fep-ssl


## Group vars configuration
See owm-fep-ssl


## ConfigDB Configuration:
__Standard__
/*/popserv/sslCertChainPathAndFile: [/opt/imail/lib/certs/im_default_certs.txt]

/*/imapserv/sslCertChainPathAndFile: [/opt/imail/lib/certs/im_default_certs.txt]

/*/mts/sslCertChainPathAndFile: [/opt/imail/lib/certs/im_default_certs.txt]
	
__New__
Only the service that is configured to use SSL/TLS will be configured/changed:

/*/popserv/sslCertChainPathAndFile: [/opt/imail/lib/certs/{{certificate_file}}]
/*/popserv/sslCipherList: [DES-CBC3-SHA:AES128-SHA:AES256-SHA:SEED-SHA:CAMELLIA128-SHA:CAMELLIA256-SHA]

/*/imapserv/sslCertChainPathAndFile: [/opt/imail/lib/certs/{{certificate_file}}]
/*/imapserv/sslCipherList: [DES-CBC3-SHA:AES128-SHA:AES256-SHA:SEED-SHA:CAMELLIA128-SHA:CAMELLIA256-SHA]

/*/mta/sslCertChainPathAndFile: [/opt/imail/lib/certs/{{certificate_file}}]
/*/mta/sslCipherList: [DES-CBC3-SHA:AES128-SHA:AES256-SHA:SEED-SHA:CAMELLIA128-SHA:CAMELLIA256-SHA]


## Verify Installation
See owm-fep-ssl