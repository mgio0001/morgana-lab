# owm-cassmeta

Role to install apache-cassandra meta package on a system and maintenance for it.
## Inventory file configuration

* master - For first cassandra meta node  its value will be true.This flag is used for selecting of cassandra meta master node. For other node don't need to define this flag.Same will be followed for multiple sites.

## Host vars configuration
This variables needs to configured properly in host_vars/site1met01, host_vars/site1met02,etc for cassandra meta installation. Same will be followed for multiple site.

* cassandra_rack - For first cassandra meta node its value will be RAC1,for second cassandra meta node its value
 will be RAC2 ,for third cassandra meta node its value will be RAC3 etc. Same will be followed for multiple site

* cassmeta_maintenance:
   - minute: "0"
     hour: "2"
     day: "*"
     month: "*"
     weekday: "*"
  (default: Configure it for 2 AM) - defined for cron job to run at specific interval on cassandra meta node and value should be different for each node in cluster.

## Role configuration
This configurations fine tunes the cassandra in roles/owm-cassmeta/defaults/main.yml file.

Change those configurations into group_vars/<groupid> or host_vars/<hostid>, only change the recommneded value as of now, other default values remain same.

By default, roles set the below parameters into cassandra.yaml

* cluster_name: meta_cluster
* num_tokens: 1
* memory_allocator: NativeAllocator
* concurrent_reads: 48
* concurrent_writes: 48
* concurrent_counter_writes: 48
* rpc_server_type: hsha
* rpc_min_threads: 64
* rpc_max_threads: 64
* tombstone_warn_threshold: 90000
* compaction_throughput_mb_per_sec: 20
* read_request_timeout_in_ms: 10000
* write_request_timeout_in_ms: 10000
* counter_write_request_timeout_in_ms: 10000
* streaming_socket_timeout_in_ms: 3600000
* endpoint_snitch: PropertyFileSnitch
* inter_dc_tcp_nodelay: 'true'

It is possible to override the above keys (or set other), by adding a cassandra_meta_confs_aux dict in the host/group vars, e.g.

```
cassandra_meta_confs_aux:
  - { key: memtable_allocation_type, value: offheap_objects }
  - { key: commitlog_segment_size_in_mb, value: 64 }
  - { key: commitlog_total_space_in_mb, value: 48 }
  - { key: concurrent_reads, value: 16 }
  - { key: concurrent_writes, value: 16 }
  - { key: concurrent_counter_writes, value: 16 }
  - { key: rpc_min_threads, value: 16 }
  - { key: rpc_max_threads, value: 16 }
```

## Installs the apache-cassandra meta  package.

The following attributes are required:

* cassandra_source_version(default:2.0.16) - defined in the 'group_vars/cassandra'. 
* cass_user information - defined in the 'group_vars/cassandra'.
* cass_maintenance(default: true) - defined in the 'group_vars/cassandra' and required for maintenance.


