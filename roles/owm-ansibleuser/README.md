# owm-ansibleuser

Role to create the account "ansibleuser" on the controlled remote nodes.

How to proceed:
* Create the private key for the SSH authentication: ssh-keygen -t rsa -b 4096 /opt/merlin/roles/owm-ansibleuser/files/ansible_id_rsa
* If root access is available through SSH: ansible-playbook -i /path/to/inventory ansibleuser-setup.yml -u root �k (then provide root password for SSH access)
* if root access is not availble (but another sudoer is available): ansible-playbook -i staging ansibleuser-setup.yml -u <sudoer> -b �k 