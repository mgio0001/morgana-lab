# owm-camelot

## Resource control

Install and enable a slice called "camelot" for the resource control of any service assigned. Main goal is set swappiness to 0 and avoid swap usage for some services (cassandra and elasticsearch).

It is possible to monitor the slice resources with systemd-cgtop.

## Restart policy update

Install under /etc/systemd/system/{{service}}.service.d/ a configuration update in order to set Restart=always policy on some auxiliary services:
* elasticsearch
* kibana
* nagios
* nsca
* haproxy
* redis
* logstash
* nrpe
* filebeat
* metricbeat
