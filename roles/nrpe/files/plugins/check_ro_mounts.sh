#!/bin/bash
# Path: /usr/lib64/nagios/plugins/check_redis.sh
#
# Author        : Yari Latini Corazzini
# E-mail            : yari.latini@synchronoss.com
# Description   : Check Read-Only filesystem
#

# A return code of zero (0) tells Nagios everything is okay
RET_OKAY=0

# A return code of one (1) tells Nagios we're reporting a warning
# about whatever it is we're monitoring
RET_WARN=1

# A return code of two (2) tells Nagios we're reporting a critical
# error
RET_CRIT=2

# I define 3 here, but quite honestly, anything you return that
# does not fit in the 0, 1 or 2 response type (as identified
# above) is considered an unknown state.  Try to avoid this
# type if you can.
RET_UNKN=3

usage() {
cat << EOF
Usage: $0 -N <fstype> -N <fstype>...

EOF
exit $RET_UNKN
}

while [[ -n "$1" ]]; do

  v=$1

  case $v in
    -N)
      shift
      FILESYSTEM_T_LIST="${FILESYSTEM_T_LIST}|${1}"
    ;;
    -h)
      usage
    ;;
    *)
    ;;
  esac

  shift

done;

FILESYSTEM_T_LIST=$(sed 's/^|//g' <<< ${FILESYSTEM_T_LIST})

if [ -z ${FILESYSTEM_T_LIST} ]; then
  echo "No filesystem to check"
  exit $RET_WARN
fi

read -r -a RES <<< $(mount |egrep -w "${FILESYSTEM_T_LIST}" | grep -w ro 2>&1)

if [ $? == 0 ] && [ -n "${RES}" ]; then
  echo "CRITICAL: ${RES[*]}"
  exit $RET_CRIT
fi

echo "OK: all $(sed 's/|/, /g' <<< ${FILESYSTEM_T_LIST}) are rw"
exit $RET_OKAY
