#!/bin/bash
# Path: /usr/lib64/nagios/plugins/check_queue_service.sh
#
# Author        : Yari Latini Corazzini
# E-mail	    : yari.latini@synchronoss.com
# Description   : Enqueue and Delete a message in a dedicated queue
#

REST_CLIENT="/usr/local/bin/rest-client-service.sh"

# A return code of zero (0) tells Nagios everything is okay
RET_OKAY=0

# A return code of one (1) tells Nagios we're reporting a warning
# about whatever it is we're monitoring
RET_WARN=1

# A return code of two (2) tells Nagios we're reporting a critical
# error
RET_CRIT=2

# I define 3 here, but quite honestly, anything you return that
# does not fit in the 0, 1 or 2 response type (as identified
# above) is considered an unknown state.  Try to avoid this
# type if you can.
RET_UNKN=3

usage() {
cat << EOF
Usage: $0 -q <queue_name>

EOF
exit $RET_UNKN
}

while [[ -n "$1" ]]; do

  v=$1

  case $v in
    -q)
      shift
    QUEUE_NAME=${1}
    ;;
    -h)
      usage
    ;;
    *)
    ;;
  esac

  shift

done;

if [ ! -f "${REST_CLIENT}" ]; then
  echo "${REST_CLIENT} not found"
  exit $RET_UNKN
fi

if [ -z "${QUEUE_NAME}" ]; then
  echo "Missing QUEUE NAME"
  exit $RET_UNKN
fi

MESSAGE_ID=$(${REST_CLIENT} method enqueue name ${QUEUE_NAME} time $(date +%Y%m%d%H%M%S -d "1 hour ago") | grep result | awk '{ print $2 }' | sed 's/\"//g' | sed "s/'//g" 2>&1)
if [ $? != 0 ]; then
  echo "Failed: enqueue message: ${MESSAGE_ID}"
  exit $RET_CRIT
fi

sleep 10

MESSAGE_KEY=$(${REST_CLIENT} method dequeue name ${QUEUE_NAME} | grep messageKey | awk '{ print $2 }' | sed 's/\"//g' | sed "s/,//g" 2>&1)
if [ $? != 0 ]; then
  echo "Failed: fetch message: ${MESSAGE_KEY}"
  exit $RET_CRIT
fi

DELETE=$(${REST_CLIENT} method delete messageid ${MESSAGE_KEY} | grep -A1 error | tail -n 1 2>&1)
if [ $? != 0 ] || [ -n "${DELETE}" ]; then
  echo "Failed: delete message: ${DELETE}"
  exit $RET_CRIT
fi

echo "Ok: message-id=${MESSAGE_ID} - message-key=${MESSAGE_KEY} - queue=${QUEUE_NAME}"
exit $RET_OKAY
