#!/bin/bash
# Path: /usr/lib64/nagios/plugins/check_cassandra_maintenance.sh
#
# Author        : John McEvoy 
# E-mail        : john.mcevoy@synchronoss.com
# Description   : Verify the consistency of cassandra maintenance tasks
#                 Should be run on all cassandra nodes on the platform
#

# A return code of zero (0) tells Nagios everything is okay
RET_OKAY=0

# A return code of one (1) tells Nagios we're reporting a warning
# about whatever it is we're monitoring
RET_WARN=1

# A return code of two (2) tells Nagios we're reporting a critical
# error
RET_CRIT=2

# I define 3 here, but quite honestly, anything you return that
# does not fit in the 0, 1 or 2 response type (as identified
# above) is considered an unknown state.  Try to avoid this
# type if you can.
RET_UNKN=3

usage() {
cat << EOF
Usage: $0 -H <homepath> -U <user> -L <path to cassandra logs> -D <Critical num days since last maintenance> -W <Warn num days since last maintenance> -T <iacceptable delay in seconds>

 -U <user> The username to run the script as SUDO
 -L <Log File Directory> Path to cassandra maintenance logs
 -D <Critical Days> Number of days to check since last maintenance job run. It's CRITICAL if longer
 -W [Warning Days] Number of days to check since last maintenance job run. WARN if longer (default 7)
 -T [Acceptable Delay in Seconds] Acceptable number of seconds for a maintenance 'Start' task to be running hence why no corresponding "Finish' (default 43200 i.e. 12 hours)
 -h show command option 

EOF
exit $RET_UNKN
}

WARNDAYS=7
OKDELAY=3600

while [[ -n "$1" ]]; do

  v=$1

  case $v in
    -U)
      shift
      SUDOUSER=${1}
    ;;
    -L)
      shift
      LOGPATH=${1}
    ;;
    -D)
      shift
      CRITDAYS=${1}
    ;;
    -W)
      shift
      WARNDAYS=${1}
    ;;
    -T)
      shift
      OKDELAY=${1}
    ;;
    -h)
      usage
    ;;
    *)
    ;;
  esac

  shift

done;

SUDOCMD=/usr/bin/sudo
RUNUSERCMD=/sbin/runuser

if [ -z "$SUDOCMD" ]; then
  echo "Missing sudo command"
  exit $RET_UNKN;
fi

if [ -z "$RUNUSERCMD" ]; then
  echo "Missing runuser command"
  exit $RET_UNKN;
fi 

if [ -z "$SUDOUSER" ]; then
  echo "Missing user"
  exit $RET_UNKN;
fi

if [ -z "$LOGPATH" ]; then
  echo "Missing log path"
  exit $RET_UNKN
fi

if [ -z "$CRITDAYS" ]; then
  echo "Missing critical days threshold"
  exit $RET_UNKN
fi

#Get newest maintenance log file if one exists
newestfile=$(${SUDOCMD} ${RUNUSERCMD} -l ${SUDOUSER} -c "ls -t $LOGPATH/*maintenance* 2>/dev/null | head -1 | sed 's#.*/##'")

#No maintenance log file found is Critical
if [ -z $newestfile ]
then
  echo "CRIT: No maintenance log found in $LOGPATH"
  exit $RET_CRIT 
fi

#Get this hostname
hostname=`hostname`
#Get cassandra DB type
casstype=`echo $newestfile | cut -d. -f1 | cut -d_ -f1`
#Get the date from the newest maintenance log filename
filedate=`echo $newestfile | cut -d. -f3`
#Get date right now in format of date in maintenance log filename
now=`date '+%Y%m%d%H%M%S'`
#Take CRITDAYS from now to compare with date in newest maintenance log filename
critdate=`date -d "-$CRITDAYS days" '+%Y%m%d%H%M%S'`
#Take WARNDAYS from now to compare with date in newest maintenance log filename
warndate=`date -d "-$WARNDAYS days" '+%Y%m%d%H%M%S'`

#If now minus CRITDAYS is after the date of newest maintenance log then CRITICAL 
if (( $critdate > $filedate )); then
  echo "CRIT: The latest $casstype maintenance is log older than $CRITDAYS days. $LOGPATH/$newestfile"         
  exit $RET_CRIT
fi

#If now minus WARNDAYS is after the date of newest maintenance log then WARN
if (( $warndate > $filedate )); then
  echo "WARN: The latest $casstype maintenance log is older than $WARNDAYS days. $LOGPATH/$newestfile" 
  exit $RET_WARN
fi

#Maintenenace was performed in an acceptable number of days so now check the file for errors
#Have we the same number of lines with "Starting repair command #" as "Repair command #* finished"
num_starts=$(${SUDOCMD} ${RUNUSERCMD} -l ${SUDOUSER} -c "grep \"Starting repair command #\" $LOGPATH/$newestfile | wc -l")
num_finishes=$(${SUDOCMD} ${RUNUSERCMD} -l ${SUDOUSER} -c "grep \"Repair command #\" $LOGPATH/$newestfile | grep \"finished\" | wc -l")
if (( $num_starts != $num_finishes )); then
  filetimestamp=$(${SUDOCMD} ${RUNUSERCMD} -l ${SUDOUSER} -c "date -r $LOGPATH/$newestfile +%Y%m%d%H%M%S")
  okdelaydate=`date -d "-$OKDELAY seconds" '+%Y%m%d%H%M%S'`
  if (( $filetimestamp < $okdelaydate )); then
     echo "CRIT: All Starting repair commands in $casstype maintenance log have not completed. Log: $LOGPATH/$newestfile"
     exit $RET_CRIT
  fi
fi

#Now look for any errors in the maintenance log
error_strings="error|stacktrace"
num_errors=$(${SUDOCMD} ${RUNUSERCMD} -l ${SUDOUSER} -c "grep -iE \"$error_strings\" $LOGPATH/$newestfile | wc -l")

if (( $num_errors > 0 )); then
  echo "CRIT: Errors found in $casstype maintenance log. $LOGPATH/$newestfile"
  exit $RET_CRIT
fi 

echo "OK: The latest $casstype maintenance log is good. $LOGPATH/$newestfile"
exit $RET_OKAY
