#!/bin/bash
# Path: /usr/lib64/nagios/plugins/check_dir_sync.sh
#
# Author        : John McEvoy 
# E-mail        : john.mcevoy@synchronoss.com
# Description   : Verify seconday directory master is in sync with current primary
#                 Should be run on all directory masters on the platform
#

# A return code of zero (0) tells Nagios everything is okay
RET_OKAY=0

# A return code of one (1) tells Nagios we're reporting a warning
# about whatever it is we're monitoring
RET_WARN=1

# A return code of two (2) tells Nagios we're reporting a critical
# error
RET_CRIT=2

# I define 3 here, but quite honestly, anything you return that
# does not fit in the 0, 1 or 2 response type (as identified
# above) is considered an unknown state.  Try to avoid this
# type if you can.
RET_UNKN=3

usage() {
cat << EOF
Usage: $0 -H <homepath> -U <user> -T <threshold>

 -H <homepath> The INTERMAIL path.
 -U <user> The INTERMAIL username
 -T <threshold> Acceptable difference of 'lastchangenumber' between secondary and primary.
 -P [ldapPort] ldapPort (default 6006)
 -W [password] ldapPassword (default secret)
 -L [ldapUser] ldapUser (default root)
 -h show command option 

EOF
exit $RET_UNKN
}

LDAPPORT=6006
LDAPPASSWORD='secret'
LDAPUSER='root'

while [[ -n "$1" ]]; do

  v=$1

  case $v in
    -U)
      shift
      SUDOUSER=${1}
    ;;
    -H)
      shift
      SUDOHOME=${1}
    ;;
    -T)
      shift
      THRESHOLD=${1}
    ;;
    -P)
      shift
      LDAPPORT=${1}
    ;;
    -W)
      shift
      LDAPPASSWORD=${1}
    ;;
    -L)
      shift
      LDAPUSER=${1}
    ;;
    -h)
      usage
    ;;
    *)
    ;;
  esac

  shift

done;

SUDOCMD=/usr/bin/sudo
RUNUSERCMD=/sbin/runuser

if [ -z "$SUDOCMD" ]; then
  echo "Missing sudo command"
  exit $RET_UNKN;
fi

if [ -z "$RUNUSERCMD" ]; then
  echo "Missing runuser command"
  exit $RET_UNKN;
fi 

if [ -z "$SUDOUSER" ]; then
  echo "Missing user"
  exit $RET_UNKN;
fi

if [ -z "$SUDOHOME" ]; then
  echo "Missing home"
  exit $RET_UNKN
fi

if [ -z "$THRESHOLD" ]; then
  echo "Missing threshold"
  exit $RET_UNKN
fi

#Get this hostname
hostname=$(${SUDOCMD} ${RUNUSERCMD} -l ${SUDOUSER} -c "cat $SUDOHOME/config/hostname")
#Get current primary hostname
primary=$(${SUDOCMD} ${RUNUSERCMD} -l ${SUDOUSER} -c "imconfget -localconfig -m common ldapMasterHosts" 2>&1)

#is this host the primary?
if [ $hostname == $primary ]; then
   echo "OK: This is the primary"
   exit $RET_OKAY
fi

#Get lastchagnenumber of this directory host
lcnhost=$(${SUDOCMD} ${RUNUSERCMD} -l ${SUDOUSER} -c "${SUDOHOME}/bin/ldapsearch -h $hostname -p 6006 -D cn=root -w secret -s base 'objectclass=dseroot' lastchangenumber")
lcnhost=$(echo $lcnhost| cut -d\= -f2 | tr -d '\n')
#Get lastchangenumber of primary master
lcnprimary=$(${SUDOCMD} ${RUNUSERCMD} -l ${SUDOUSER} -c "${SUDOHOME}/bin/ldapsearch -h $primary -p 6006 -D cn=root -w secret -s base 'objectclass=dseroot' lastchangenumber")
lcnprimary=$(echo $lcnprimary|cut -d\= -f2 |tr -d '\n')

#Check if this master is ahead or behind the primary
if [ $lcnprimary -gt $lcnhost ]; then
   difference=`expr $lcnprimary - $lcnhost`
   primaryahead="1"
elif [ $lcnprimary -lt $lcnhost ]; then
   difference=`expr $lcnhost - $lcnprimary`
   primaryahead="0"
else
   difference="0"
fi  

#If the 'lastchangenumber' is within the threshold of the primary lastchangenumber then it is in sync
if [ $difference -lt "$THRESHOLD" ]; then
   echo "OK: This master is in sync with the primary"
   #Delete any previous lastchangenumber tmp files
   if [ -e /tmp/lastchangenumber ]; then
      rm -f /tmp/lastchangenumber
   fi
   if [ -e /tmp/lastchangenumberprim ]; then
      rm -f /tmp/lastchangenumberprim
   fi
   exit $RET_OKAY
fi

#So this directory is out of sync.
#If the primary is ahead of this secondary
#If the file '/tmp/lastchangenumber' is found then the directory is already known to be out of sync. Is it progressing?
if [ $primaryahead -eq 1 ]; then
   if [ -e /tmp/lastchangenumber ]; then
      lastchangenumber=`cat /tmp/lastchangenumber`
      #if out of sync but progressing
      if [ $lcnhost -gt $lastchangenumber ]; then
         echo "$lcnhost" > /tmp/lastchangenumber
         echo "WARN: This master is not in sync with the primary but is progressing"
         exit $RET_WARN
      fi
      #if already out of sync but not progressing
      echo "$lcnhost" > /tmp/lastchangenumber
      echo "FAILED: This master is not in sync with the primary but is not progressing"
      exit $RET_CRIT   
   fi
fi

#If this secondary is ahead of the primary 
#If the file '/tmp/lastchangenumberprim' is found then the directory is already known to be out of sync. Is it progressing?
if [ $primaryahead -eq 0 ]; then
   if [ -e /tmp/lastchangenumberprim ]; then
      lastchangenumberprim=`cat /tmp/lastchangenumberprim`
      #if out of sync but progressing
      if [ $lcnprimary -gt $lastchangenumberprim ]; then
         echo "$lcnprimary" > /tmp/lastchangenumberprim
         echo "WARN: This master is behind the primary but the primary is progressing"
         exit $RET_WARN
      fi
      #if already out of sync but not progressing
      echo "$lcnprimary" > /tmp/lastchangenumberprim
      echo "FAILED: This master is behind the primary but the primary is not progressing"
      exit $RET_CRIT
   fi
fi

#Getting this far means this directory is out of sync for first time
if [ $primaryahead -eq 1 ]; then
   echo "$lcnhost" > /tmp/lastchangenumber
else
   echo "$lcnprimary" > /tmp/lastchangenumberprim
fi
echo "WARN: This master has recently become out of sync with the primary" 
exit $RET_WARN
