#!/bin/bash
#
# Author        : N.Hashimoto (original version), Yari Latini Corazzini (runuser based version for multi node check)
# E-mail        : mrhashnao@gmail.com, yari.latini@synchronoss.com
# Description   : Verify node joining cassandra multinode cluster, and
#                 send alert if the number of live node is less than the specified number.
#                 nodetool is executed with sudo runuser -l <cassuser> -c cassandra-<casstype>/cassandra/bin/nodetool
#                 It is required to enable nrpe user as sudoer for the runuser command, e.g with /etc/sudoers.d/nrpe:
#
#               Defaults:nrpe   !requiretty
#               Runas_Alias     MXUSERS = cassqueue,cass
#               Cmnd_Alias      HSCMD = /sbin/runuser -l cassqueue *, /sbin/runuser -l cass *
#               nrpe            ALL = (root) NOPASSWD: HSCMD
#
#                Some additional checks for latency, Dropped Mutations and Exception have been added
# ------------------------------------------------------------
# functions
# ------------------------------------------------------------
# print help
usage() {
cat << EOF
Usage: $0 -w <warning> -c <critical> -U <user> -T <meta or blob>

 -w <warning> alert warning state, if the number of live nodes is less than <warning>.
 -c <critical> alert critical state, if the number of live nodes is less than <critical>.
 -W <latency_warning> alert warning state, if the R/W latency is more than <latency_warning>.
 -C <latency_critical> alert critical state, if the R/W latency is more than <latency_critical>.
 -U <cassuser> the user running the cassandra node
 -T <casstype> the cassandra node type (meta or blob)
 -h show command option
 -p ports (default: gtn, g-gossip t-thrift n-native)
 -V show command version

EOF
exit 3
}

# Checking the status, outputting the nagios status code
check_status() {
case $retval in
  0 )
  echo "OK - Live Node:$live_node - Dropped Mutations:$mutations - Exceptions:${exceptions} - ${latencymsg} - Ports(G,T,N):${gossip},${thrift},${native}"
  exit 0
  ;;

  1 )
  echo "WARNING - Live Node:$live_node - Dropped Mutations:$mutations - Exceptions:${exceptions} - ${latencymsg} - Ports(G,T,N):${gossip},${thrift},${native}"
  exit 1
  ;;

  2 )
  echo "CRITICAL - Live Node:$live_node - Dropped Mutations:$mutations - Exceptions:${exceptions} - ${latencymsg} - Ports(G,T,N):${gossip},${thrift},${native}"
  exit 2
  ;;

  3 )
  echo "UNKNOWN - Live Node:$live_node - Dropped Mutations:$mutations - Exceptions:${exceptions} - ${latencymsg} - Ports(G,T,N):${gossip},${thrift},${native}"
  exit 3
  ;;

esac
}


# ------------------------------------------------------------
# variables
# ------------------------------------------------------------
export LANG=C
opt_v=1.00
date=$(date '+%Y%m%d')

host="localhost"
port="7199"

#PROGNAME=`basename $0`
#PROGPATH=`echo $0 | sed -e 's,[\\/][^\\/][^\\/]*$,,'`
#REVISION=`echo '$Revision: 1749 $' | sed -e 's/[^0-9.]//g'`
. /usr/lib64/nagios/plugins/utils.sh

# option definitions
while getopts "c:w:C:W:U:T:hV" opt ; do
  case $opt in
  c )
  critical="$OPTARG"
  ;;

  C )
  lcritical="$OPTARG"
  ;;

  w )
  warning="$OPTARG"
  ;;

  W )
  lwarning="$OPTARG"
  ;;

  h )
  usage
  ;;

  V )
  echo "`basename $0` $opt_v" ; exit 0
  ;;

  U )
  cassuser="$OPTARG"
  ;;

  T )
  casstype="$OPTARG"
  ;;

  p )
  cassports="$OPTARG"
  ;;

  * )
  usage
  ;;

  esac
done
shift `expr $OPTIND - 1`

# verify warning and critical are number
expr $warning + 1 >/dev/null 2>&1
if [ "$?" -lt 2 ]; then
  true
else
  echo "-c <critical> $critical must be number."
  exit 3
fi

expr $critical + 1 >/dev/null 2>&1
if [ "$?" -lt 2 ]; then
  true
else
  echo "-c <critical> $critical must be number."
  exit 3
fi

# verify warning is less than critical
if [ "$warning " -lt "$critical" ]; then
  echo "-w <warning> $warning must be less than -c <critical> $critical."
  exit 3
fi


# ------------------------------------------------------------
# begin script
# ------------------------------------------------------------
# check the number of live node, status and performance
live_node=$(sudo runuser -l ${cassuser} -c "cassandra-${casstype}/cassandra/bin/nodetool ring" | grep -c 'Up')
#verbose=($(sudo runuser -l ${cassuser} -c "cassandra-${casstype}/cassandra/bin/nodetool ring" | awk '/Up/ {print $1":"$4","$5","$6$7","$8 " " }'))
#performance=($(sudo runuser -l ${cassuser} -c "cassandra-${casstype}/cassandra/bin/nodetool ring" | awk '/Up/ {print "Load_"$1"="$6$7,"Owns_"$1"="$8}'))
mutations=($(sudo runuser -l ${cassuser} -c "cassandra-${casstype}/cassandra/bin/nodetool tpstats"|grep "^MUTATION"|awk '{ print $2 }'))
read -r -a infodata <<< $(sudo runuser -l ${cassuser} -c "cassandra-${casstype}/cassandra/bin/nodetool info"|egrep '^Gossip active|^Thrift active|^Native Transport active|^Exceptions')
gossip=${infodata[3]}
thrift=${infodata[7]}
native=${infodata[11]}
exceptions=(${infodata[14]})
read -r -a latency <<< $(sudo runuser -l ${cassuser} -c "cassandra-${casstype}/cassandra/bin/nodetool cfstats "| egrep '\s+Read Latency:|\s+Write Latency:|Keyspace:')

# unless live node is number, reply unknown code
expr $live_node + 1 >/dev/null 2>&1
if [ "$?" -lt 2 ]; then
  true
else
  retval=3
fi

# verify the number of live node is less than critical, warning
if [ "$live_node" -le "$critical" ]; then
  retval=2
else
  if [ "$live_node" -le "$warning" ]; then
    retval=1
  else
    retval=0
  fi
fi

if [ -z "$lwarning" ]; then lwarning=10; fi
if [ -z "$lcritical" ]; then lcritical=100; fi

# check latency
latency_check=0
latencymsg="Latency:"
for i in $(seq 0 $((${#latency[*]} / 10 - 1))); do
  ksp=${latency[$((1+$i*10))]}
  rl=$(printf "%.0f" ${latency[$((4+$i*10))]})
  wl=$(printf "%.0f" ${latency[$((8+$i*10))]})
  if [ $wl == "nan" ]; then wl=0; fi
  if [ $rl == "nan" ]; then rl=0; fi
  latencymsg="${latencymsg} ${ksp}:R=${rl}ms:W=${wl}ms"
  if [ $wl -gt $lwarning ] || [ $rl -gt $lwarning ]; then
    latency_check=1
    latencymsg="${latencymsg}:WARN"
  fi
  if [ $wl -gt $lcritical ] || [ $rl -gt $lcritical ]; then
    latency_check=1
    latencymsg="${latencymsg}:CRIT"
  fi
done

# check for warnings
if [ ${retval} -lt 1 ]; then
  if [ ${mutations} -gt 0 ] || [ ${exceptions} -gt 0 ] || [ $latency_check == 1 ]; then
    retval=1
  fi
fi

# check for criticals
if [ "${gossip}" != "true" ] || [ "${thrift}" != "true" -a "${native}" != "true" ] || [ $latency_check == 2 ]; then
  retval=2
fi

check_status
ten
