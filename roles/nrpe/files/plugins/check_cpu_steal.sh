#!/bin/bash
# Path: /usr/lib64/nagios/plugins/check_mx_ping.sh
#
# Author        : Yari Latini Corazzini 
# E-mail	    : yari.latini@synchronoss.com
# Description   : Check CPU steal and iostats, useful for virtual hosts
#

# A return code of zero (0) tells Nagios everything is okay
RET_OKAY=0

# A return code of one (1) tells Nagios we're reporting a warning
# about whatever it is we're monitoring
RET_WARN=1

# A return code of two (2) tells Nagios we're reporting a critical
# error
RET_CRIT=2

# I define 3 here, but quite honestly, anything you return that
# does not fit in the 0, 1 or 2 response type (as identified
# above) is considered an unknown state.  Try to avoid this
# type if you can.
RET_UNKN=3

usage() {
cat << EOF
Usage: $0 -w <iostat_warning,steal_warning> -c <iostat_critical,steal_critical>

EOF
exit $RET_UNKN
}

while [[ -n "$1" ]]; do

  v=$1

  case $v in
    -w)
      shift
    WARNING_T_LIST=${1}
    ;;
    -c)
      shift
    CRITICAL_T_LIST=${1}
    ;;
    -h)
      usage
    ;;
    *)
    ;;
  esac

  shift

done;

if [ -z "$WARNING_T_LIST" ]; then
        echo "Missing WARNING threshold"
        exit $RET_UNKN;
fi

if [ -z "$CRITICAL_T_LIST" ]; then
        echo "Missing CRITICAL threshold"
        exit $RET_UNKN;
fi

IFS_TMP=$IFS
IFS=","
read -r -a WARNINGS <<< "${WARNING_T_LIST}"
read -r -a CRITICALS <<< "${CRITICAL_T_LIST}"
IFS=$IFS_TMP

IOSTAT=$(which iostat)


RES=$(${IOSTAT} -c | grep -A 1 avg | tail -n 1 2>&1)

read user_v nice_v system_v iowait_v steal_v idle_v <<< "${RES}"

echo "Iowait: ${iowait_v}% - CPU Steal: ${steal_v}%"

if [ $(($(printf "%.0f" $iowait_v))) -gt $((${CRITICALS[0]})) ] || [ $(($(printf "%0.f" $steal_v))) -gt $((${CRITICALS[1]})) ]; then
  exit $RET_CRIT
elif [ $(($(printf "%.0f" $iowait_v))) -gt $((${WARNINGS[0]})) ] || [ $(($(printf "%0.f" $steal_v))) -gt $((${WARNINGS[1]})) ]; then
  exit $RET_WARN
fi

exit $RET_OKAY
