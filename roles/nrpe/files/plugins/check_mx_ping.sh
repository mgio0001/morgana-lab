#!/bin/bash
# Path: /usr/lib64/nagios/plugins/check_mx_ping.sh
#
# Author        : Yari Latini Corazzini 
# E-mail	    : yari.latini@synchronoss.com
# Description   : Execute the imservping command in order to check Mx services status
#

# A return code of zero (0) tells Nagios everything is okay
RET_OKAY=0

# A return code of one (1) tells Nagios we're reporting a warning
# about whatever it is we're monitoring
RET_WARN=1

# A return code of two (2) tells Nagios we're reporting a critical
# error
RET_CRIT=2

# I define 3 here, but quite honestly, anything you return that
# does not fit in the 0, 1 or 2 response type (as identified
# above) is considered an unknown state.  Try to avoid this
# type if you can.
RET_UNKN=3

usage() {
cat << EOF
Usage: $0 -H <homepath> -U <user>

 -H <homepath> The INTERMAIL path.
 -U <user> The INTERMAIL username
 -h show command option 

EOF
exit $RET_UNKN
}

while [[ -n "$1" ]]; do

  v=$1

  case $v in
    -U)
      shift
      SUDOUSER=${1}
    ;;
    -H)
      shift
      SUDOHOME=${1}
    ;;
    -h)
      usage
    ;;
    *)
    ;;
  esac

  shift

done;

SUDOCMD=/usr/bin/sudo
RUNUSERCMD=/sbin/runuser

if [ -z "$SUDOCMD" ]; then
  echo "Missing sudo command"
  exit $RET_UNKN;
fi

if [ -z "$RUNUSERCMD" ]; then
  echo "Missing runuser command"
  exit $RET_UNKN;
fi 

if [ -z "$SUDOUSER" ]; then
  echo "Missing user"
  exit $RET_UNKN;
fi

if [ -z "$SUDOHOME" ]; then
  echo "Missing home"
  exit $RET_UNKN
fi

MXPING=${SUDOHOME}/bin/imservping

RES=$(${SUDOCMD} ${RUNUSERCMD} -l ${SUDOUSER} -c ${MXPING} 2>&1)

if [ $? == 0 ]; then
  echo "OK: ${RES}"
  exit $RET_OKAY
else
  echo "FAILED: ${RES}"
  exit $RET_CRIT
fi
