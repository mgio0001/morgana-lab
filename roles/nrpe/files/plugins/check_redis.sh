#!/bin/bash
# Path: /usr/lib64/nagios/plugins/check_redis.sh
#
# Author        : Yari Latini Corazzini 
# E-mail	    : yari.latini@synchronoss.com
# Description   : Check Redis metrics
#

# A return code of zero (0) tells Nagios everything is okay
RET_OKAY=0

# A return code of one (1) tells Nagios we're reporting a warning
# about whatever it is we're monitoring
RET_WARN=1

# A return code of two (2) tells Nagios we're reporting a critical
# error
RET_CRIT=2

# I define 3 here, but quite honestly, anything you return that
# does not fit in the 0, 1 or 2 response type (as identified
# above) is considered an unknown state.  Try to avoid this
# type if you can.
RET_UNKN=3

usage() {
cat << EOF
Usage: $0 -w <blocked_clients,rejected_connections,evicted_keys> -c <blocked_clients,rejected_connections,evicted_keys>

EOF
exit $RET_UNKN
}

while [[ -n "$1" ]]; do

  v=$1

  case $v in
    -w)
      shift
    WARNING_T_LIST=${1}
    ;;
    -c)
      shift
    CRITICAL_T_LIST=${1}
    ;;
    -h)
      usage
    ;;
    *)
    ;;
  esac

  shift

done;

if [ -z "$WARNING_T_LIST" ]; then
        echo "Missing WARNING threshold"
        exit $RET_UNKN;
fi

if [ -z "$CRITICAL_T_LIST" ]; then
        echo "Missing CRITICAL threshold"
        exit $RET_UNKN;
fi

IFS_TMP=$IFS
IFS=","
read -r -a WARNINGS <<< "${WARNING_T_LIST}"
read -r -a CRITICALS <<< "${CRITICAL_T_LIST}"
IFS=$IFS_TMP

read -r -a redisdata <<< $(/bin/redis-cli info |egrep 'evicted_keys|rejected_connections|blocked_clients' | sed 's/\r//g')

blocked_clients=($(cut -d: -f 2 <<< ${redisdata[0]}))
rejected_connections=($(cut -d: -f 2 <<< ${redisdata[1]}))
evicted_keys=($(cut -d: -f 2 <<< ${redisdata[2]}))

if [ $? != 0 ]; then
  echo "Failed: $?"
  exit $RET_UNKN
fi

if [ ${blocked_clients} -gt $((${CRITICALS[0]})) ] || [ ${rejected_connections} -gt $((${CRITICALS[1]})) ] || [ ${evicted_keys} -gt $((${CRITICALS[2]})) ]; then
  echo "CRITICAL: blocked_clients=${blocked_clients} - rejected_connections=${rejected_connections} - evicted_keys=${evicted_keys}"
  exit $RET_CRIT
elif [ ${blocked_clients} -gt $((${WARNINGS[0]})) ] || [ ${rejected_connections} -gt $((${WARNINGS[1]})) ] || [ ${evicted_keys} -gt $((${WARNINGS[2]})) ]; then
  echo "WARNING: blocked_clients=${blocked_clients} - rejected_connections=${rejected_connections} - evicted_keys=${evicted_keys}"
  exit $RET_WARN
fi

echo "OK: blocked_clients=${blocked_clients} - rejected_connections=${rejected_connections} - evicted_keys=${evicted_keys}"
exit $RET_OKAY
