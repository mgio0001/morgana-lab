# nrpe

Ansible role to handle the installation of the Nagios NRPE Daemon.

## Role Information

This role gives you the ability to deploy global plugins.
This can be done by putting plugins into [`files/plugins`](files/plugins)

## Role Variables

  * *nrpe_bind_address*: 127.0.0.1
  * *nrpe_port*: 5666
  * *nrpe_allowed_hosts*: 127.0.0.1
  * *nrpe_pid*: /var/run/nrpe/nrpe.pid
  * *nrpe_user*: nrpe
  * *nrpe_group*: nrpe
  * *nrpe_repo_redhat*: epel
  * *nrpe_service*: nrpe
  * *nrpe_dir*: /etc/nagios
  * *nrpe_include_dir*: /etc/nrpe.d

## Dependencies

epel

## Example Playbook

```yaml
- hosts: servers
  roles:
     - nrpe
   vars:
     nrpe_allowed_hosts: 192.168.0.1,127.0.0.1
```

## SELinux policy

Some check (like check_mx_ping) will need some policy configuration on hosts where SELinux is running in enforcing mode. 
Below the procedure to use in order to update the .pp files by starting from the .te files (e.g. for imservping-nrpe policy):

```  
     
     # Compile the module
     $ checkmodule -M -m -o imservping-nrpe.mod imservping-nrpe.te

     # Create the package
     $ semodule_package -o imservping-nrpe.pp -m imservping-nrpe.mod
    
     # Load the module into the kernel (this will be performed by the ansible playbook)
     $ semodule -i imservping-nrpe.pp
```
