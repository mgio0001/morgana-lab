# hostname

This role sets the hostname based on inventory data.

It is important that every host resolve its own hostname when mx services start. This role skips the host that have the attribute is_alias_host: true (e.g. cassandra nodes having multiple instances) since it will lead to inconsistency or unexpected hostname resolution.

# test

Verify hostname has been configured propertly:

```
ansible -i morgana-lab -m shell -a 'hostnamectl|head -n 1' all
```
that will provide an output like below
```
mx921qdata | SUCCESS | rc=0 >>
   Static hostname: mx921data

mx921data | SUCCESS | rc=0 >>
   Static hostname: mx921data

mx921service | SUCCESS | rc=0 >>
   Static hostname: mx921service

mx921access | SUCCESS | rc=0 >>
   Static hostname: mx921access

```

NB: in the above example mx921qdata is an alias host for the queue ring (see [Deployment of a 3 nodes LAB](https://bitbucket.org/openwave/morgana/wiki/Deployment%20of%20a%203%20nodes%20LAB) for further details)
It is possible to skip the hostname configuration (e.g. for the cassandra blobstore nodes) by settings

skip_hostnames: "true"

or

is_alias_host: "true"
