# owm-dac

Role to install or update dac package and configure it.

The DAC service is a java webapplication running on a tomcat that will be installed under $INTERMAIL/DAC/tomcat/.

# vars configuration

```yaml
# name of the dac package to install
# it will be downloaded from {{owm_tar_url}}/
# So make sure you package is available from the configured tarball repository
dac_build_version: '14'

# Tomcat configuration details
tomcat_version: '8.5.6'
tomcat_http_port: '8081'
tomcat_https_port: '8444'

# Configure the secure="true" flag into the http connector
# to make the session cookie secure (won't work on http://) and change the 
# Server: header in the response to "Web Server" in order to hide the
# tomcat build. Default: false
# With false server="Web Server" and secure="true" attribute will be removed.
dac_secure: 'true'  
```