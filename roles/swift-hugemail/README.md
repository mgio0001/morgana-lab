# swift-hugemail

Role to install and configure Swift for use by hugemail

## Needed per-customer configuration in group_vars/swift-hugemail

mountpoint_base: All loopback and physical filesystems will be mounted under this directory.  This is to simplify rsync/swift configuration of the filesystems

need_loopback_disks: set to "true" if loopback disks are to be created to store swift data

use_physical_disks: set to "true" to use existing physical disk devices to store swift data

loopback_disks: A structure showing how to create/mount the loopback filesystems.  The same filesystems *MUST* be created on all nodes in the cluster, so this should be specified at the group level, not the host level.  e.g.
loopback_disks:
    - file_path: Where to put the backing file.  e.g. /opt/store/object-volume1
      size: How big to make the backing file, e.g. 20G
      device_path: What device to create under /dev for this logical device.  e.g. /dev/loop1
      inode_size: Value to pass to the '-i' parameter on mkfs.xfs.  e.g. size=1024
      mountpoint_subdir: What directory under {{ mountpoint_base }} to use for this filesystem, e.g. loop1

physical_disks: A structure showing which disk devices to use and where to mount them when using physical disks instead of loopback disks.  No partitioning or filesystem creation is done.  e.g.
physical_disks:
    - mountpoint_subdir: What directory under {{ mountpoint_base }} to use for this filesystem, e.g. loop1
      label: The label to create for the filesystem
      fs_type: The filesystem type, e.g. xfs

The value for following two parameters can be generatd by doing 'openssl rand -hex 10' or 'od -t x8 -N 8 -A n </dev/random'.  Use different values for prefix and suffix.  Must be unique to this deployment.
swift_hash_path_prefix: A random number used by hash_path to offer a bit more security when generating hashes for paths.
swift_hash_path_suffix: A random number used by hash_path to offer a bit more security when generating hashes for paths.
