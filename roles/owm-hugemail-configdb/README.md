# owm-hugemail-configdb

Role to add hugemail specific configuration keys to Mx9 config database

## Configuration options with default values

The following 5 values are put into the key with the same name in config.db under /*/hugemail/
cassandraPort: 9042 
cassandraSchemaMigratorEnabled: "true"
s3AccessKey: "admin:admin"
s3BucketName: "hugemail"
s3SecretKey: "admin"
nginx_swift_port: 55015
