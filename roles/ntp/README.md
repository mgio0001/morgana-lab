# ntp

This role installs and configure the ntp services on the controlled hosts. THis is a mandatory tasks in order to properly work with Mx platform, Cassandra as any other ring technology.

# configuration

This role check for the ntp_config_server list (that is usually configured for group all or <site>). While it is possible to use public ntp service on a production deployment it is highly recommended to have dedicated service in the local network (every cloud / virtualization platform provides its own ntp services exposed to the virtual hosts), e.g.
```yaml
ntp_config_server: [ '0.europe.pool.ntp.org', '1.europe.pool.ntp.org' ]
```