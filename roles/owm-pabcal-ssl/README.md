# owm-pabcal-ssl

Adds a certificate (Server Certificate, Server Key and CA chain) to the PAB and CAL server and enables SSL/TLS.


## Required customer specific data
It requires an archive file with the following files in it:
- Server Key
- Server Certificate
- CA Certificate Chain

The archive is retrieved from {{owm_tar_url}}.


## Group vars configuration
The name of the archive and the files in it are configured in:
group_vars/pabcal.

Additionally the SSL deployment and configuration can be enabled/disabled:
ssl_pabcal:    True | False

The setting below defines it all existing certificates shall be removed ("True" is recommended). If they are not removed they will exist in parallel!
remove_existing_certificates_pabcal: True | False


## Verify Installation
Verify with openssl, make seu DN is correct and no error is shown. Replace with your hostname:

__CalDAV:__
openssl s_client -port 5232 -host a1mss01

__CardDAV:__
openssl s_client -port 9092 -host a1mss01
