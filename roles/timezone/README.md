# timezone

Sets the timezone.

# Example Playbook

```
---
- hosts: all
  roles:
  - timezone

  vars:
   timezone: America/Los_Angeles
```

# Test

Verify the right timezone is now set on the controlled hosts, e.g.
```
ansible -i morgana-lab -b -m shell -a 'timedatectl|grep "Time zone"' all -o
```
which will return (assuming you set timezone: Europe/Rome in your group_vars/<site> or group_vars/all)
```
mx921access | SUCCESS | rc=0 | (stdout)        Time zone: Europe/Rome (CEST, +0200)
mx921service | SUCCESS | rc=0 | (stdout)        Time zone: Europe/Rome (CEST, +0200)
mx921data | SUCCESS | rc=0 | (stdout)        Time zone: Europe/Rome (CEST, +0200)
mx921qdata | SUCCESS | rc=0 | (stdout)        Time zone: Europe/Rome (CEST, +0200)
```
