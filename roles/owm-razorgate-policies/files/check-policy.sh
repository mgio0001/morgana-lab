#!/bin/bash
#
# Perform very high-level validation of the policies.  Also create a tgz
# of the files necessary to install the policy and save it in a path
# nominated by ansible.  This appears to be faster than using
# the "copy" module in ansible and with_items, which is rather
# slow.
#
# Expected environment variables:
#
# - RG_POLICY_BASEPATH
# - RG_POLICIES
# - RG_DEFAULT_POLICY
# - RG_VALID_SMTP_CHECKPOINTS
# - RG_POLICY_TMP_ARCHIVE
#
# RG_POLICIES and RG_VALID_SMTP_CHECKPOINTS should be comma separated lists
#
# Under RG_POLICY_BASEPATH there should be several directories:
#
# - Policies
# - Rules
# - Lists
#

DEBUG=0

if [ $DEBUG -ne 0 ]; then
    exec 2>> /tmp/check-policy.log
    set -x
fi

if [ "x${RG_POLICY_BASEPATH}" = "x" ]; then
    echo "RG_POLICY_BASEPATH needs to be defined"
    exit 1
fi

if [ "x${RG_POLICIES}" = "x" ]; then
    echo "RG_POLICIES needs to be defined"
    exit 1
fi

if [ "x${RG_VALID_SMTP_CHECKPOINTS}" = "x" ]; then
    echo "RG_VALID_SMTP_CHECKPOINTS needs to be defined"
    exit 1
fi

if [ "x${RG_DEFAULT_POLICY}" = "x" ]; then
    echo "RG_DEFAULT_POLICY needs to be defined"
    exit 1
fi

if [ "x${RG_POLICY_TMP_ARCHIVE}" = "x" ]; then
    echo "RG_POLICY_TMP_ARCHIVE needs to be defined"
    exit 1
fi

if [ ! -d "${RG_POLICY_BASEPATH}" ]; then
    echo "RG_POLICY_BASEPATH is not a valid directory"
    exit 1
fi

if echo "${RG_VALID_SMTP_CHECKPOINTS}" | grep -vq ','; then
    echo "RG_VALID_SMTP_CHECKPOINTS should be a comma separated list of multiple checkpoints"
    exit 1
fi

IFS=', ' read -r -a VALID_CHECKPOINTS <<< "${RG_VALID_SMTP_CHECKPOINTS}"
IFS=', ' read -r -a POLICIES <<< "${RG_POLICIES}"
DEFAULT_POLICY_FOUND=0

validate_checkpoint()
{
    local CHECKPOINT=$1
    local POLICY=$2
    local VALID_CP

    CHECKPOINT=$(echo "${CHECKPOINT}" | tr "[:lower:]" "[:upper:]")
    for VALID_CP in "${VALID_CHECKPOINTS[@]}"; do
        if [ "x${VALID_CP}" = "x${CHECKPOINT}" ]; then
            return
        fi
    done
    echo "Checkpoint \"${CHECKPOINT}\" does not exist for \"${POLICY}\""
    exit 1
}

declare -A NEEDED_POLICIES
declare -A NEEDED_RULES

for POLICY in "${POLICIES[@]}"; do
    POLICY_FILE="${RG_POLICY_BASEPATH}/Policies/${POLICY}.policy"
    if [ ! -f "${POLICY_FILE}" ]; then
        echo "Policies/${POLICY}.policy not found under base path"
        exit 1
    fi

    if [ ! -r "${POLICY_FILE}" ]; then
        echo "Policies/${POLICY}.policy not readable"
        exit 1
    fi

    if [ "x${RG_DEFAULT_POLICY}" = "x${POLICY}" ]; then
        DEFAULT_POLICY_FOUND=1
    fi

    NEEDED_POLICIES["${POLICY}.policy"]=1

    IFS=', ' read -r -a POLICY_CHECKPOINTS <<< $(grep -v '^#' ${RG_POLICY_BASEPATH}/Policies/${POLICY}.policy | awk '{print $1}' | sort -u | xargs echo)
    unset IFS
    if [ $DEBUG -ne 0 ]; then
        printf '%s,\n' "${POLICY_CHECKPOINTS[@]}" | xargs echo 1>&2
    fi
    IFS=', ' read -r -a POLICY_RULES <<< $(grep -v '^#' ${RG_POLICY_BASEPATH}/Policies/${POLICY}.policy | awk '{print $2}' | sort -u | xargs echo)
    unset IFS
    if [ $DEBUG -ne 0 ]; then
        printf '%s,' "${POLICY_RULES[@]}" 1>&2
    fi

    for CHECKPOINT in ${POLICY_CHECKPOINTS}; do
        validate_checkpoint "${CHECKPOINT}" "${POLICY}"
    done

    for RULE in "${POLICY_RULES[@]}"; do
        RULE_FILE="${RG_POLICY_BASEPATH}/Rules/${RULE}.rule"
        if [ ! -e "${RULE_FILE}" ]; then
            echo "Rules/${RULE}.rule not found under base path for policy ${POLICY}"
            exit 1
        fi

        if [ ! -r "${RULE_FILE}" ]; then
            echo "Rules/${RULE}.rule not readable under base path for policy ${POLICY}"
            exit 1
        fi

        if [ ! -s "${RULE_FILE}" ]; then
            echo "Rules/${RULE}.rule zero length under base path for policy ${POLICY}"
            exit 1
        fi
        NEEDED_RULES["${RULE}.rule"]=1
    done
done

if [ $DEFAULT_POLICY_FOUND -eq 0 ]; then
    echo "Default policy \"${RG_DEFAULT_POLICY}\" not on the list of policies"
    exit 1
fi

rm -f "${RG_POLICY_TMP_ARCHIVE}"
tar -zcf "${RG_POLICY_TMP_ARCHIVE}" \
    -C "${RG_POLICY_BASEPATH}/Policies" $(printf "%s " ${!NEEDED_POLICIES[@]} | sed 's/ $//') \
    -C "${RG_POLICY_BASEPATH}/Rules" $(printf "%s " ${!NEEDED_RULES[@]} | sed 's/ $//')
