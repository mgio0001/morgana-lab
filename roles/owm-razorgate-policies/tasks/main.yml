---
- name: Determine access network IP of Razorgate
  set_fact:
   rgAccessHost: "{% if hostvars[inventory_hostname]['ethernet_interfaces'] is defined %}{% for interface in hostvars[inventory_hostname]['ethernet_interfaces'] %}{% if interface['vlan'] is defined and interface['vlan'] == 'access' %}{{ interface['address'] }}{% endif %}{% endfor %}{% endif %}"
  tags: [ 'owm-razorgate', 'owm-razorgate-policy' ]

- name: Check access network IP of Razorgate has been found
  fail: msg="No access network found in ethernet_interfaces list for host, or no ethernet_interfaces list found"
  when: rgAccessHost==""
  tags: [ 'owm-razorgate', 'owm-razorgate-policy' ]

- name: Calculate policy archive path
  set_fact:
    rg_policy_tmp_archive: "{{ rg_policy_tmp_archive_prefix }}_{{ inventory_hostname }}.tgz"
  delegate_to: localhost
  tags: [ 'owm-razorgate', 'owm-razorgate-policy' ]

- name: Calculate policy install path
  set_fact:
    rg_policy_install_path: "{{ rg_policy_install_path_prefix }}/{{ inventory_hostname }}"
  delegate_to: localhost
  tags: [ 'owm-razorgate', 'owm-razorgate-policy' ]

- name: Copy check script
  copy:
    src: check-policy.sh
    dest: /tmp/check-policy.sh
    mode: 0755
  delegate_to: localhost
  tags: [ 'owm-razorgate', 'owm-razorgate-policy' ]

- name: Check policy is valid
  shell: /tmp/check-policy.sh
  environment:
    RG_POLICY_BASEPATH: "{{ rg_policy_basepath }}"
    RG_POLICIES: "{{ rg_policies|join(',') }}"
    RG_DEFAULT_POLICY: "{{ rg_default_policy }}"
    RG_VALID_SMTP_CHECKPOINTS: "{{ rg_valid_smtp_checkpoints|join(',') }}"
    RG_POLICY_TMP_ARCHIVE: "{{ rg_policy_tmp_archive }}"
  ignore_errors: True
  register: CHECK_POLICY
  delegate_to: localhost
  tags: [ 'owm-razorgate', 'owm-razorgate-policy' ]

# - debug: var=CHECK_POLICY
#   tags: [ 'owm-razorgate', 'owm-razorgate-policy' ]

- name: Check policy is valid
  fail:
    msg: "{{ CHECK_POLICY.stdout }}"
  when: CHECK_POLICY.rc != 0
  delegate_to: localhost
  tags: [ 'owm-razorgate', 'owm-razorgate-policy' ]

- name: Create list of needed rules
  set_fact:
    rg_needed_files: "{{ CHECK_POLICY.stdout.split(',') | sort}}"
  when: CHECK_POLICY.rc == 0
  delegate_to: localhost
  tags: [ 'owm-razorgate', 'owm-razorgate-policy' ]

# - debug: var=rg_needed_files
#   tags: [ 'owm-razorgate', 'owm-razorgate-policy' ]

- name: Clean up any existing policy directory
  file:
    path: "{{ rg_policy_install_path }}"
    state: absent
  tags: [ 'owm-razorgate', 'owm-razorgate-policy' ]

- name: Create directory to hold policy files on the RazorGate
  file:
    path: "{{ rg_policy_install_path }}"
    state: directory
    owner: "{{ emailmx_user[0].username }}"
    group: "{{ emailmx_user[0].username if users_create_per_user_group else users_group }}"
    mode: 0755
  tags: [ 'owm-razorgate', 'owm-razorgate-policy' ]

- name: Copy policy files to the RazorGate
  unarchive:
    src: "{{ rg_policy_tmp_archive }}"
    dest: "{{ rg_policy_install_path }}"
    owner: "{{ emailmx_user[0].username }}"
    group: "{{ emailmx_user[0].username if users_create_per_user_group else users_group }}"
    mode: 0644
  tags: [ 'owm-razorgate', 'owm-razorgate-policy' ]

- name: Create default policy file on the RazorGate
  lineinfile:
    path: "{{ rg_policy_install_path }}/default"
    create: yes
    owner: "{{ emailmx_user[0].username }}"
    group: "{{ emailmx_user[0].username if users_create_per_user_group else users_group }}"
    state: present
    line: "{{ rg_default_policy }}"
    regexp: "^.*$"
    mode: 0644
  tags: [ 'owm-razorgate', 'owm-razorgate-policy' ]

- name: Install the policy
  shell: "source {{ emailmx_user[0].home }}/.profile ; {{ razorgate_basepath }}/mira/usr/bin/mgr.sh -s {{ rgAccessHost }} PLUGIN BMAPI MUX POLICY INSTALL {{ rg_policy_install_path }}"
  become: yes
  become_user: "{{ emailmx_user[0].username }}"
  register: bmapi_install
  ignore_errors: True
  tags: [ 'owm-razorgate', 'owm-razorgate-policy' ]

# - debug: var=bmapi_install
#   tags: [ 'owm-razorgate', 'owm-razorgate-policy' ]

- name: Check for mgr.sh errors
  debug:
    msg:
      - "{{ bmapi_install.stderr_lines }}"
  when: 'bmapi_install.rc !=0'
  tags: [ 'owm-razorgate', 'owm-razorgate-policy' ]

- name: Check for errors from smtpd
  debug:
    msg:
      - "{{ bmapi_install.stdout_lines }}"
  when: '"ERROR" in bmapi_install.stdout'
  tags: [ 'owm-razorgate', 'owm-razorgate-policy' ]

- name: Check for success from smtpd
  debug:
    msg:
      - "{{ bmapi_install.stdout_lines }}"
  when: 'bmapi_install.rc == 0 and "ERROR" not in bmapi_install.stdout'
  tags: [ 'owm-razorgate', 'owm-razorgate-policy' ]

- name: Check that the policy installed successfully
  fail:
    msg:
      - "Policy failed to install."
  when: 'bmapi_install.rc !=0 or "ERROR" in bmapi_install.stdout'
  tags: [ 'owm-razorgate', 'owm-razorgate-policy' ]

