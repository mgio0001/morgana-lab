#!/usr/bin/perl
#
require "log-archiver.pm";
use File::Copy;
use File::Basename;

foreach my $log_dir (keys %logDirs) {
        trace ("---------- START ELABORATION");
        if (! -d $log_dir) {
                trace ("$log_dir does not exist, skipping");
                next;
        }
        my $suffix = $logDirs{$log_dir};
        trace ("---------- Examining $log_dir, suffix is $suffix");

        $count = 0;
	# To manage linked directories add a "/" to path
        $log_dir.="/";
        open(IFD, "find $log_dir -maxdepth 1 -type f |") or die "find";
        while(<IFD>) {
                chomp;
		# Skip if extension is not configured
		$extFileFound = "";	
	 	$extFound = 0;	
		$fileName = basename($_);
	 	foreach my $extFile (@extensions) {
			if ($fileName =~ /$extFile/) {	
				$extFound = 1;
				$extFileFound = $extFile;
			}
		}
		if ($extFound == 0) {
			trace ("Skipped $_ because has no extensions configured in list");
			next;
		}
		# Skip if is not a file (skip links,directories)
		next unless -f $_;
		# Skip if file is not older then $daysToLeave days
                $modified = (-M $_) * 24 ;
                if ($modified < 24 * $daysToLeave) {
                        next;
                }
		# Finally skip if file is the last one of its kind
                my ($service,$machine,$fileDate,$ext) = split /\./, $_;
		chomp ($lastLogFile = `ls -trL $service*$extFileFound* | tail -1`);
                if ($lastLogFile eq $_) {
                        trace ("Skipping $_ because is last file of its kind");
                        next;
                }
                push @files, $_;
                $count++; 
	}
        $files = join(' ', @files);
        if ($count > 0) {
                my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
                my $dateString = sprintf("%04d\/%02d\/%02d", $year + 1900, $mon + 1, $mday);
                my $timeString = sprintf("%02d\_%02d\_%02d", $hour, $min, $sec);
                my $folderString = "$archiveDir/$suffix/$dateString";
                system("mkdir -p $folderString");
                foreach (@files) {
                        trace ("Moving $_ in $folderString");
                        system("mv $_ $folderString");
                }
            	trace ("Gzipping using command gzip -q $folderString/*");
                system("gzip -q $folderString/*");
                if ($? != 0) {
                        trace ("ERROR - gzip returned $?");
                }
        }
        close(IFD);
        undef @files;
}

sub getTimeLOG {
        my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();	
	my $str = sprintf("%04d\/%02d\/%02d %02d\:%02d:%02d", $year + 1900, $mon + 1, $mday, $hour, $min, $sec);
        return $str;
}

sub trace {
        my $str = $_[0];
        if($enableTrace) {
                open (LOG_, ">>$logFile");
                print LOG_ getTimeLOG()." $str\n";
                close LOG_;
        }
}	
