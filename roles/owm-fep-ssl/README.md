# owm-fep-ssl

Adds a certificate (Server Certificate, Server Key and CA chain) to the backend server and enables SSL/TLS. It can be enabled for POP, IMAP and SMTP. Each service can be enabled individually.


## Required customer specific data
It requires an archive file with the following files in it:
- Server Key
- Server Certificate
- CA Certificate Chain

The archive is retrieved from {{owm_tar_url}}.


## Group vars configuration
The name of the archive and the files in it are configured in:
group_vars/fep.

Additionally the SSL deployment and configuration can be enabled/disabled:
ssl_pop:       True | False
ssl_imap:      True | False
ssl_smtp:      False | True


## Verify Installation
Verify with openssl, make seu DN is correct and no error is shown. Replace with your hostname:

__POP:__
openssl s_client -port 995 -host a1fep01

__IMAP:__
openssl s_client -port 993 -host a1fep01

__SMTP:__
openssl s_client -port 465 -host a1fep01
