# hosts

This role generates the /etc/hosts for all nodes. It relies on an agreement
of what interface is used for what purpose. This follows from the reference
architecture.

This role also check the configured VIP and cluster and make sure their names are resolved.

Failing in properly configuring the required keys for this role may result in a not working deployment because hosts and services will fail to route requests internally.

The hosts will be added to the /etc/hosts for internal resolution by adding the suffix -adm -rep -svc depending on the vlan their IP are assigned.

This role also adds the "primarymaster" entry that is used only by the provisioner and its failover monitoring.

This role also adds the cassandra cluster hostnames to all the cassandra nodes (based on the ring they belong to).

This role skips the host that have the attribute is_alias_host: true (e.g. cassandra nodes having multiple instances).

# configuration

Make sure your inventory file will be configured with the ip addresses connected to the admin vlan (recommended) or to the service vlan (alternative if you can't afford a dedicated vlan for admninistration).

Verify for each host in the platform you have a proper file host_vars/<hostname> having at least the below configuration (data nodes will have vlan: data rather then vlan: access and service nodes will have only admin and service):

```
ethernet_interfaces:
 - device: enp0s3
   vlan: admin
   address: 10.0.2.9
 - device: enp0s9
   vlan: access
   address: 10.0.4.4
 - device: enp0s8
   vlan: service
   address: 10.0.3.4
```

It will be also required to configure the clusterid and assign it to the nodes affected by the CSC deployment, so make sure all the hosts that belong a cluster has its clusterid attribute configured in their host_vars/<hostname> file, e.g.

```
clusterId: cluster01
```

The available clusters will have to be defined also in the group_vars/mx or group_vars/all file in order to enable this role to perform the proper lookup
```
# We'll have a single cluster
clusters:
  - cluster01
#  - cluster02
```

The nodes where the affinity manager is installed will be also listening on a floating IP managed by PCS, so make sure those hosts also include the VIP attribute pointing to that floating IP. This role will include a <clusterId>-VIP entry in the /etc/hosts file pointing to that floating IP, e.g.

```
VIP: VIP_AM_ADDR
component: 'mss'
```
which will create an entry in the /etc/hosts file of every host like below

```
10.49.2.3       cluster01-VIP cluster01
```

(where 10.49.2.3 is the floating IP or in a LAB deployment the same IP of the designed affinity manage node)

Below an example of a generated /etc/hosts file for a [small replica LAB](https://bitbucket.org/openwave/morgana/wiki/Deployment%20of%20a%20small%20production%20replica%20LAB):

```
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
# BEGIN MERLIN This file is managed by Merlin. Do not edit directly.

10.49.2.40      a1data01-adm
10.49.2.40      a1data01-rep
10.49.2.40      a1data01-svc a1data01 blobcluster metacluster pimcluster
10.49.2.41      a1data02-adm
10.49.2.41      a1data02-rep
10.49.2.41      a1data02-svc a1data02 blobcluster metacluster pimcluster
10.49.2.43      a1data03-adm
10.49.2.43      a1data03-rep
10.49.2.43      a1data03-svc a1data03 blobcluster metacluster pimcluster
10.49.2.44      a1data04-adm
10.49.2.44      a1data04-rep
10.49.2.44      a1data04-svc a1data04 blobcluster metacluster pimcluster
10.49.2.3       a1am01-adm
10.49.2.3       a1am01-svc a1am01
10.49.2.3       cluster01-VIP cluster01
10.49.2.34      a1am02-adm
10.49.2.34      a1am02-svc a1am02
10.49.2.34      a1am02-VIP
10.49.2.45      a1swift01-adm
10.49.2.45      a1swift01-rep
10.49.2.45      a1swift01-svc a1swift01
10.49.2.46      a1swift02-adm
10.49.2.46      a1swift02-rep
10.49.2.46      a1swift02-svc a1swift02
10.49.2.47      a1swift03-adm
10.49.2.47      a1swift03-rep
10.49.2.47      a1swift03-svc a1swift03
10.49.2.55      a1mon01-adm
10.49.2.55      a1mon01-svc a1mon01
10.49.2.56      a1mon02-adm
10.49.2.56      a1mon02-svc a1mon02
10.49.2.35      a1que01-adm
10.49.2.35      a1que01-svc a1que01
10.49.2.36      a1que02-adm
10.49.2.36      a1que02-svc a1que02
10.49.2.40      a1qdata01-adm
10.49.2.40      a1qdata01-rep
10.49.2.40      a1qdata01-svc a1qdata01  qcluster
10.49.2.41      a1qdata02-adm
10.49.2.41      a1qdata02-rep
10.49.2.41      a1qdata02-svc a1qdata02  qcluster
10.49.2.43      a1qdata03-adm
10.49.2.43      a1qdata03-rep
10.49.2.43      a1qdata03-svc a1qdata03  qcluster
10.49.2.44      a1qdata04-adm
10.49.2.44      a1qdata04-rep
10.49.2.44      a1qdata04-svc a1qdata04  qcluster
10.49.2.252     a1fep01-adm
10.49.2.252     a1fep01-dmz
10.49.2.252     a1fep01-svc a1fep01
10.49.2.253     a1fep02-adm
10.49.2.253     a1fep02-dmz
10.49.2.253     a1fep02-svc a1fep02
10.49.2.39      a1dir01-adm
# primary master pointer
10.49.2.39      primarymaster
10.49.2.39      a1dir01-svc a1dir01
10.49.2.4       a1dir02-adm
10.49.2.4       a1dir02-svc a1dir02
10.49.2.53      a1prov01-adm
10.49.2.53      a1prov01-svc a1prov01
10.49.2.38      a1mss01-adm
10.49.2.38      a1mss01-svc a1mss01
10.49.2.37      a1mss02-adm
10.49.2.37      a1mss02-svc a1mss02
10.49.2.255     a1ux01-adm
10.49.2.255     a1ux01-dmz
10.49.2.255     a1ux01-svc a1ux01
10.49.2.254     a1ux02-adm
10.49.2.254     a1ux02-dmz
10.49.2.254     a1ux02-svc a1ux02
10.49.2.51      a1dac01-adm
10.49.2.51      a1dac01-svc a1dac01
# haproxy hosts
127.0.0.1 pab_cluster
127.0.0.1 cal_cluster
# END MERLIN

```

