# owm-dirserv-ha

Add common settings to the configuration database.  Run separately from the
rest as these are not per-host settings.  Only runs on the master config
server, which is also directory server

## Needed per-customer configuration in group_vars/directory
switchOverPriorityHosts: "<list of hosts>" or "auto" to let role take care of building the list, first entry will be the directory node having "primary=true", then list will include other directory nodes by following the inventory order (use ansible -m debug -a msg="{{inventory_hostname}}" directory to check the complete list). If unset "auto" will be used.

## Configuration settings possible via group vars or host vars, with current
## default values
switchoverCheckInterval: 10
switchoverThresholdTimeFrame: 100
switchoverThresholdCount: 2
scriptListeningPort: 7000

## Add "imautoswitchover" to the monitoredServers configuration.
## Needs to be set where the master config server can see it
monitor_imautoswitchover: False

## Add "imswitchoverlistener" to the monitoredServers configuration
## Needs to be set where the master config server can see it
monitor_imswitchoverlistener: False
