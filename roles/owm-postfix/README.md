# owm-postfix

Role to change running postfix in order to listen locally and not using port 25.
This role has to be executed before installing owm-mta.

The operating system may need to send notifications though MTA. By default RHEL provides postfix, but it could cause conflict and has to be configured on all the hosts in the "null client" mode where all messages are routed to a designed MTA that will act as gateway for the platform. That gateway usually is the mgmt node that usually accesses and get accessed by all the hosts through the admin VLAN.

## Host vars configuration

* postfix_port: 2525
* internal_relayhost: '[mgmt-node]'
