#!/bin/bash

MASTER_HOST=dir-b-01
OUTPUT_DIR=/opt/merlin/output

ansible -i /opt/merlin/production -b -m shell -a 'cat /opt/imail/config/config.db' ${MASTER_HOST}  > ${OUTPUT_DIR}/config.db-snapshots/config.db-`date +%Y%m%d@%H%M%S`