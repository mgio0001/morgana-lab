#!/bin/bash

# This script prepares the platform for Ansible and Merlin
# by retrieving the public keys from the managed hosts
# and creating the ansibleuser accounts with sudo privilege
# on the remote hosts.
#
# author: yari.latini@synchronoss.com
# doc: https://bitbucket.org/openwave/morgana/wiki/morgana-00-bootstrap.sh

# different playbook could have different tags from Merlin you may want to skip
# set below the comma separated list of tags you want to ignore
TAGBL=""
# EXTRA_OPTS this task requies
# -k is required in order to ask for root password on bootstrap
EXTRA_OPTS="-k -u root"
# this is the playbook you will execute
PLAYBOOK="morgana-bootstrap.yml"

# parse options and execute
source $(dirname $0)/lib.sh
