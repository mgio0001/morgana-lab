#!/bin/bash

# this script execute the deployment of the mss blobstore rings
#
# author: yari.latini@synchronoss.com

# Since blob nodes are on aliases, there is no need to apply the prod00-common.sh script
# because you have already executed prod01-mssmetacass.sh and so its prod00-common.sh

# Applies or updates basic configuration on the managed hosts including:
# 1) install JDK
# 2) install and configure cassandra rings for mss service

# REINSTALL PROCEDURE
#
# in order to reset the changes from this playbook, execute the command below:
# kill the cassandra processes
#
# ansible -i <inventory-file> -b -m shell -a 'systemctl status cassblob-cassblob1 cassblob-cassblob2 cassblob-cassblob3' -b <target_host>
#
# remove data and accounts 
# ansible -i <inventory-file> -b -m user -a 'name=cassblob1 state=absent remove=yes' <target_host>
# ansible -i <inventory-file> -b -m user -a 'name=cassblob2 state=absent remove=yes' <target_host>
# ansible -i <inventory-file> -b -m user -a 'name=cassblob3 state=absent remove=yes' <target_host>
# ansible -i <inventory-file> -b -m shell -a 'rm -rf /opt/{blob1,blob2,blob3}/{cassandra-blob,.oracle_jre_usage,scripts,.cassandra}' <target_host>

# different playbook could have different tags from Merlin you may want to skip
# set below the comma separated list of tags you want to ignore
TAGBL="--skip-tags=hostname,timezone,userpermissions,users,ntp,dns,limits,hosts"
# this is the playbook you will execute
PLAYBOOK="morgana-cassblob.yml"

# parse options and execute
source $(dirname $0)/lib.sh

# Useful blob commands
#
# TEST MAINTENANCE =====================================================
#
# Execute the maintenance scripts node by node (recommended after the deployment in order to test the nodes and their maintenance)
# ansible -i <inventory-file> -b -m shell -a 'bash /opt/{{cass_user_id}}/scripts/cassblob_maintenance.sh' cassblob -f1
#
# Check cronjobs have been installed and scheduled properly (very weekday are well distributed among nodes and servers)
# ansible -i <inventory-file> -b -m shell -a 'cat /var/spool/cron/{{cass_user_id}}' cassblob -f1
#
# Update only the maintenance scripts
# ./prod01-cassblob.sh --tags maintenance,owm-cassblob-init
#
# Reload the schema on the master node (existing keyspace has to be removed - DATA WILL BE LOST)
# ./prod01-cassblob.sh --tags owm-cassblob-loadschema,owm-cassblob-init --target <master_cassandra_node>
#
# Update the configuration files
# ./prod01-cassblob.sh --tags owm-cassblob-init,owm-cassblob-confupdates
#
# START / STOP SERVICES ================================================
#
# Restart the instances
# ansible -i <inventory-file> -b -m shell -a 'systemctl restart cassblob-{{cass_user_id}}' cassblob
#
# Execute nodetool <command>
# ansible -i <inventory-file> -b -m shell -a 'runuser -l {{cass_user_id}} -c "cassandra-blob/cassandra/bin/nodetool <command>"' cassblob
#
# MONITORING COMMANDS ==================================================
#
# Check ring status
# ansible -i <inventory-file> -b -m shell -a 'runuser -l {{cass_user_id}} -c "cassandra-blob/cassandra/bin/nodetool ring KeyspaceBlobStore"' cassandra1-hrc-a
#
# Check compaction status
# ansible -i <inventory-file> -b -m shell -a 'runuser -l {{cass_user_id}} -c "cassandra-blob/cassandra/bin/nodetool compactionstats"' cassblob
#
# Check thread pool statistics
# ansible -i <inventory-file> -b -m shell -a 'runuser -l {{cass_user_id}} -c "cassandra-blob/cassandra/bin/nodetool tpstats"' cassblob
#
# Check Snapshots
# ansible -i <inventory-file> -b -m shell -a 'runuser -l {{cass_user_id}} -c "cassandra-blob/cassandra/bin/nodetool listsnapshots"' cassblob
# ansible -i <inventory-file> -b -m shell -a '{% for m in data_file_directories %} find {{ m }} -name snapshots -type d {% endfor %}' cassblob
#
# Check log files
# ansible -i <inventory-file> -b -m shell -a 'runuser -l {{cass_user_id}} -c "egrep -v \"INFO|^(\s+)|^java\.\" cassandra-blob/log/system.log"' cassblob
#
# Check disk usage
# ansible -i <inventory-file> -b -m shell -a '{% for m in data_file_directories %} df -h {{ m }} {% endfor %}' cassblob
#
# CONFIGURATION CHECKS =================================================
#
# Check for specific configuration parameters (e.g. seeds)
# ansible -i <inventory-file> -b -m shell -a 'runuser -l {{cass_user_id}} -c "grep seeds cassandra-blob/cassandra/conf/cassandra.yaml"' cassblob
#
# Check cassandra-topology.properties
# ansible -i <inventory-file> -m shell -a 'grep -v '^#' /opt/{{cass_user_id}}/cassandra-blob/cassandra/conf/cassandra-topology.properties' cassblob -b
#
# Check Keyspace (or run any other cqlsh command)
# ansible -i <inventory-file> -b -m shell -a 'runuser -l {{cass_user_id}} -c "cassandra-blob/cassandra/bin/cqlsh {{inventory_hostname}} 9043 -k KeyspaceBlobStore -e DESCRIBE\ KEYSPACE"' <cassandra-blob-node>


