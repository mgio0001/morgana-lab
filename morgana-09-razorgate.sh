#!/bin/bash

# This script will execute the deployment of the RazorGate
#
# author: gary.palmer@synchronoss.com

# REINSTALL PROCEDURE
#
# in order to reset the changes from this playbook, execute the command below (stop primary as last if you're reinstalling everything):
#
# ansible -i <inventory-file> -b -f1 -m shell -a 'systemctl stop razorgate' <target_host>
#
# remove installation
# ansible -i morgana-lab -b -m shell -a 'yum erase -y razorgate' <target-host>
#
# You also need to delete the following keys from config.db (for Mx9 installs):
# /<target-host>/sysadmin/counters_opt
# /<target-host>/sysadmin/counters_run: [on]
# /<target-host>/sysadmin/kas_opt
# /<target-host>/sysadmin/kas_run
# /<target-host>/sysadmin/kav_opt
# /<target-host>/sysadmin/kav_run
# /<target-host>/sysadmin/smtp_opt
# /<target-host>/sysadmin/smtp_run
# /<target-host>/sysadmin/sophosas_opt
# /<target-host>/sysadmin/sophosas_run
# /<target-host>/sysadmin/sophosav_opt
# /<target-host>/sysadmin/sophosav_run
# /<target-host>/sysadmin/vadeas_opt
# /<target-host>/sysadmin/vadeas_run
#
# Failure to remove the keys will prevent reinstallation as immgrserv seems
# to re-create some of the directories under $MIRA_ROOT when restarted,
# so the mx-installer role would prevent the RG RPM from installing.
# You may also want to delete other keys that are present to reset back
# to defaults, both for the host and the group.
#
# different playbook could have different tags from Merlin you may want to skip
# set below the comma separated list of tags you want to ignore
TAGBL=""
# TODO: Document tags used for different parts of the RG install/configure
#       (e.g. policy install, limits, etc)
# this is the playbook you will execute
PLAYBOOK="morgana-razorgate.yml"

# parse options and execute
source $(dirname $0)/lib.sh

