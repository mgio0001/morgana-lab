#!/bin/bash

# this script execute the deployment of the various post-install tasks
# - log archiver
#
# author: yari.latini@synchronoss.com

# different playbook could have different tags from Merlin you may want to skip
# set below the comma separated list of tags you want to ignore
TAGBL="--skip-tags=owm-webtop-rpmforge,owm-webtop-webmail,hostname,timezone,userpermissions,users,ntp,dns,limits,hosts"
# this is the playbook you will execute
PLAYBOOK="morgana-postinstall.yml"

# parse options and execute
source $(dirname $0)/lib.sh


# Useful commands
#
# START / STOP SERVICES ================================================
#
# CONFIGURATION CHANGES ================================================
#
# MONITORING COMMANDS ==================================================
#
# CHECK MAINTENANCE ====================================================
#
# CONFIGURATION CHECKS =================================================
#
