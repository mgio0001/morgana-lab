#!/bin/bash

# this script execute the deployment of the mx services
# owm-cal
# owm-common
# owm-confserv
# owm-dircacheserv
# owm-dirserv
# owm-eureka
# owm-extserv
# owm-imapserv
# owm-mos
# owm-mss
# owm-mta
# owm-nginx
# owm-pab
# platformtools
# owm-popserv
# owm-queue-admin
# owm-queue-search
# owm-queue-service
# owm-queue
#
# author: yari.latini@synchronoss.com

# different playbook could have different tags from Merlin you may want to skip
# set below the comma separated list of tags you want to ignore
TAGBL="--skip-tags=owm-webtop-rpmforge,owm-webtop-webmail,hostname,timezone,userpermissions,users,ntp,dns,limits,hosts"
# this is the playbook you will execute
PLAYBOOK="morgana-mx-installer.yml"

# parse options and execute
source $(dirname $0)/lib.sh


# Useful commands
#
# START / STOP SERVICES ================================================
#
# CONFIGURATION CHANGES ================================================
#
# MONITORING COMMANDS ==================================================
#
# Check process are up
# ansible -i morgana-lab -b -f1 -m shell -a 'runuser -l imail -c imservping' <target-host>
# 
# Check processes (imconfserv and immgrsev MUST be up and running)
# ansible -i morgana-lab -b -f1 -m shell -a 'runuser -l imail -c imservping' <target-host>
#
# Check current log files for Errors/Warning/Critical messages
# ansible -i morgana-lab -b -f1 -m shell -a 'runuser -l imail -c "find log/ -name \*.log -type l | xargs grep -v Note | imlogprint -m"' mss
#
# CHECK MAINTENANCE ====================================================
#
# CONFIGURATION CHECKS =================================================
#
